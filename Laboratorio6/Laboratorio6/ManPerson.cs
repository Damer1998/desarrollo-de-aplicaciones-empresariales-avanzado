﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MaterialSkin;
// SQL
using System.Data.SqlClient;

namespace Laboratorio6
{
    public partial class ManPerson : MaterialSkin.Controls.MaterialForm
    {

        SqlConnection con;
        DataSet ds = new DataSet();
        DataTable tablePerson = new DataTable();



        public ManPerson()
        {
            InitializeComponent();
        }

        //Fomr ManPerson
        private void ManPerson_Load(object sender, EventArgs e)
        {
            String str = "Server=LAPTOP-1T0G2E8E;DataBase=Escuela;Integrated Security=true;";
            con = new SqlConnection(str);
        }

        // Boton Listar
        private void btnListar_Click(object sender, EventArgs e)
        {
           
            String sql = "SELECT * FROM Person";
            SqlCommand cmd = new SqlCommand(sql, con);

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = cmd;

            // Se llena el dataset con la tabla Person
            adapter.Fill(ds, "Person");

            // se le asigna la tabla del dataset a un objetivo table para trabajar con el
            tablePerson = ds.Tables["Person"];

            dgvListado.DataSource = tablePerson;
            dgvListado.Update();
       



        }


        // Boton Insertar 
        private void btnInsertar_Click(object sender, EventArgs e)
        {

            SqlCommand cmd = new SqlCommand("InsertPerson", con);

            cmd.Parameters.Add("@LastName", SqlDbType.VarChar, 50, "LastName");
            cmd.Parameters.Add("@FirstName",SqlDbType.VarChar, 50, "FirstName");
            cmd.Parameters.Add("@HireDate", SqlDbType.Date).SourceColumn = "HireDate";
            cmd.Parameters.Add("@EnrollmentDate", SqlDbType.Date).SourceColumn = "EnrollmentDate";

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.InsertCommand = cmd;
            adapter.InsertCommand.CommandType = CommandType.StoredProcedure;

            //Creamos una fila nueva la cual insertamos en la bd
            DataRow fila = tablePerson.NewRow();
            fila["LastName"] = txtLastName.Text;
            fila["FirstName"] = txtFirstName.Text;
            //fila["HireDate"] = Convert.ToDateTime(txtHireDate.Text);
            //fila["EnrollmentDate"] = Convert.ToDateTime(txtEnrollmentDate.Text);

            //fila["HireDate"] = txtHireDate.Text;
            //fila["EnrollmentDate"] = txtEnrollmentDate.Text;

            if (txtHireDate.Checked) fila["HireDate"] = txtHireDate.Text;
            else fila["HireDate"] = DBNull.Value;

            if (txtEnrollmentDate.Checked) fila["EnrollmentDate"] = txtEnrollmentDate.Text;
            else fila["EnrollmentDate"] = DBNull.Value;

            // Se le agrega a la dataset
            tablePerson.Rows.Add(fila);

            adapter.Update(tablePerson);

        }

        // Botono Modificar
        private void btnModificar_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("UpdatePerson", con);

            cmd.Parameters.Add("@PersonID", SqlDbType.VarChar).SourceColumn = "PersonID";
            cmd.Parameters.Add("@LastName", SqlDbType.VarChar).SourceColumn = "LastName";
            cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).SourceColumn = "FirstName";

            cmd.Parameters.Add("@HireDate", SqlDbType.Date).SourceColumn = "HireDate";
            cmd.Parameters.Add("@EnrollmentDate", SqlDbType.Date).SourceColumn = "EnrollmentDate";

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.UpdateCommand = cmd;
            adapter.UpdateCommand.CommandType = CommandType.StoredProcedure;

            //Problemas reconocimiento Id
            //Creamos un array de DataRow en donde se almacena en la fila conocida de la busqueda de ID
            DataRow[] fila = tablePerson.Select("PersonID = '" + txtPersonID.Text + "'");
  
            fila[0]["LastName"] = txtLastName.Text;
            fila[0]["FirstName"] = txtFirstName.Text;
            //fila[0]["HireDate"] = Convert.ToDateTime(txtHireDate.Text);
            // fila[0]["EnrollmentDate"] = Convert.ToDateTime(txtEnrollmentDate.Text);
            //fila[0]["HireDate"] = txtHireDate.Text;
            //fila[0]["EnrollmentDate"] = txtEnrollmentDate.Text;

            if (txtHireDate.Checked) fila[0]["HireDate"] = txtHireDate.Text;
            else fila[0]["HireDate"] = DBNull.Value;

            if (txtEnrollmentDate.Checked) fila[0]["EnrollmentDate"] = txtEnrollmentDate.Text;
            else fila[0]["EnrollmentDate"] = DBNull.Value;


            // Se Actualiza
            adapter.Update(tablePerson);
        }


        // Boton Eliminar
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("DeletePerson", con);
            cmd.Parameters.Add("@PersonID", SqlDbType.VarChar).SourceColumn = "PersonID";

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.DeleteCommand = cmd;
            adapter.DeleteCommand.CommandType = CommandType.StoredProcedure;

            //Busqueda de Fila
            DataRow[] fila = tablePerson.Select("PersonID = '" + txtPersonID.Text + "'");

            //Eliminar la tabla de la fila 
            tablePerson.Rows.Remove(fila[0]);


            //Actualizar el dataset
            adapter.Update(tablePerson);

        }

        // Boton Ordenar por Apellido
        private void btnApe_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(tablePerson);
            //dv.Sort = "LastName ASC";
            dv.RowFilter = "LastName = '" + txtLastName.Text + "'";
            dgvListado.DataSource = dv;
        }


        //Boton Ordenar por Codigo
        private void brnCodigo_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(tablePerson);
            dv.RowFilter = "PersonID = '" + txtPersonID.Text + "'" ;
            dgvListado.DataSource = dv;
        }
        //Boton Ordenar por Nombre
        private void btnNombre_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(tablePerson);
            dv.RowFilter = "FirstName = '" + txtFirstName.Text + "'";
            dgvListado.DataSource = dv;
        }

        // Boton Ordenar por Contrato
        private void btnContrato_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(tablePerson);
            dv.RowFilter = "HireDate = '" + txtHireDate.Text + "'";
            dgvListado.DataSource = dv;
        }
        // Boton Ordenar por Incsripcion
        private void btnInscripcion_Click(object sender, EventArgs e)
        {
            DataView dv = new DataView(tablePerson);
            dv.RowFilter = "EnrollmentDate = '" + txtEnrollmentDate.Text + "'";
            dgvListado.DataSource = dv;
        }

        //Botono Buscar
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            con.Open();
            String sql = "SELECT * FROM Person where " + comboBoxBusqueda.Text + " like '%" + textBusqueda.Text + "%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            DataTable dt = new DataTable();
            dt.Load(reader);

            dgvListado.DataSource = dt;
            dgvListado.Refresh();
            con.Close();
        }

        //Selection Changed de dgvListado
        private void dgvListado_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvListado.SelectedRows.Count > 0)
            {
                txtPersonID.Text = dgvListado.SelectedRows[0].Cells[0].Value.ToString();
                txtLastName.Text = dgvListado.SelectedRows[0].Cells[1].Value.ToString();
                txtFirstName.Text = dgvListado.SelectedRows[0].Cells[2].Value.ToString();

                string hireDate  = dgvListado.SelectedRows[0].Cells[3].Value.ToString();
                if (String.IsNullOrEmpty(hireDate))

                    txtHireDate.Checked = false;
                    
                else
                    txtHireDate.Text = hireDate;
                //txtHireDate.CustomFormat = "dd-MM-yyyy";

                string enrollmentDate = dgvListado.SelectedRows[0].Cells[4].Value.ToString();
                if (String.IsNullOrEmpty(enrollmentDate))
                    txtEnrollmentDate.Checked = false;
                else
                    txtEnrollmentDate.Text = enrollmentDate;
               // txtEnrollmentDate.CustomFormat = "dd-MM-yyyy";
            }
        }




        // Contrato 
        private void txtHireDate_ValueChanged(object sender, EventArgs e)
        {
            txtHireDate.CustomFormat = "dd-MM-yyyy";
        }
        // Validar NUll Contrato
        private void txtHireDate_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Back) || (e.KeyCode == Keys.Delete))
            {
                txtHireDate.CustomFormat = " ";
            }
        }
        //Inscrisiopn
        private void txtEnrollmentDate_ValueChanged(object sender, EventArgs e)
        {
            txtEnrollmentDate.CustomFormat = "dd-MM-yyyy";
        }


        // Validar NUll
        private void txtEnrollmentDate_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Back) || (e.KeyCode == Keys.Delete))
            {
                txtEnrollmentDate.CustomFormat = " ";
            }
        }
    }
}
