﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Diseño de Form
using MaterialSkin;

// Usar capa negocio
using Negocio;


namespace Presentacion
{
    public partial class placas : MaterialSkin.Controls.MaterialForm
    {

        //Ruta
        private N_Placas objetoCN = new N_Placas();
        private string PK_vehiculos = null;  // primary key
        private bool Editar = false;




        public placas()
        {
            InitializeComponent();
        }


        //Limpieza de Datos
        private void limpiarForm()
        {
            txtplacas.Clear();
            txtcaractplacas.Clear();
           // txtnombrecli.Clear();
            

        }

        // Listado Inicio y Actulizado
        private void MostrarVehiculos()
        {

            N_Placas objeto = new N_Placas();
            dataGridView1.DataSource = objeto.MostrarVehiculos();
        }

        private void MostrarClientesRegistro()
        {

            N_Placas objeto = new N_Placas();
            txtnombrecli.DataSource = objeto.MostrarClientesRegistro();

            

        }




        // Listar Vehiculo

        private void placas_Load(object sender, EventArgs e)
        {
            MostrarVehiculos();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //INSERTAR
            if (Editar == false)
            {
                try
                {
                    objetoCN.InsetarVehiculos(txtplacas.Text, txtcaractplacas.Text,txtnombrecli.Text,txtestadoplacas.Text);
                    MessageBox.Show("Se inserto correctamente");
                    MostrarVehiculos();
                    limpiarForm();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se pudo insertar los datos por: " + ex);
                }
            }
            //EDITAR
            if (Editar == true)
            {

                try
                {
                    objetoCN.EditarVehiculos(txtplacas.Text, txtcaractplacas.Text, txtnombrecli.Text, txtestadoplacas.Text, PK_vehiculos);
                    MessageBox.Show("Se edito correctamente");
                    MostrarVehiculos();
                    limpiarForm();
                    Editar = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se pudo editar los datos por: " + ex);
                }
            }

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Editar = true;
                txtplacas.Text = dataGridView1.CurrentRow.Cells["placa"].Value.ToString();
                txtcaractplacas.Text = dataGridView1.CurrentRow.Cells["caracteristicas"].Value.ToString();
                txtnombrecli.Text = dataGridView1.CurrentRow.Cells["nomcli"].Value.ToString();
                txtestadoplacas.Text = dataGridView1.CurrentRow.Cells["estado"].Value.ToString();
                PK_vehiculos = dataGridView1.CurrentRow.Cells["PK_vehiculos"].Value.ToString();


            }
            else
                MessageBox.Show("Seleccione una fila por favor");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                PK_vehiculos = dataGridView1.CurrentRow.Cells["PK_vehiculos"].Value.ToString();
                objetoCN.EliminarVehiculos(PK_vehiculos);
                MessageBox.Show("Eliminado Cliente correctamente");
                MostrarVehiculos();
            }
            else
                MessageBox.Show("Seleccione una fila por favor");
        }

        private void txtnombrecli_SelectedIndexChanged(object sender, EventArgs e)
        {

            MostrarClientesRegistro();
        }
    }
}
