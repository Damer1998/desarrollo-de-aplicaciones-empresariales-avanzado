﻿namespace Presentacion
{
    partial class parqueo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(parqueo));
            this.btnClienteRegistrado = new System.Windows.Forms.Button();
            this.btnIngresoCliente = new System.Windows.Forms.Button();
            this.btnIngresoPlaca = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.labelTema = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnIngreso = new System.Windows.Forms.Button();
            this.btnSalida = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblHora = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.horafecha = new System.Windows.Forms.Timer(this.components);
            this.plagascli = new System.Windows.Forms.Label();
            this.txtplacas = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtinvnum = new System.Windows.Forms.TextBox();
            this.btnBuscarPlaca = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtnomcli = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.btnReimprimirTicket = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClienteRegistrado
            // 
            this.btnClienteRegistrado.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnClienteRegistrado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClienteRegistrado.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteRegistrado.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnClienteRegistrado.Location = new System.Drawing.Point(990, 99);
            this.btnClienteRegistrado.Name = "btnClienteRegistrado";
            this.btnClienteRegistrado.Size = new System.Drawing.Size(201, 46);
            this.btnClienteRegistrado.TabIndex = 91;
            this.btnClienteRegistrado.Text = "Buscar Cliente R";
            this.btnClienteRegistrado.UseVisualStyleBackColor = false;
            this.btnClienteRegistrado.Click += new System.EventHandler(this.btnReimprimirTicket_Click);
            // 
            // btnIngresoCliente
            // 
            this.btnIngresoCliente.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnIngresoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIngresoCliente.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngresoCliente.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnIngresoCliente.Location = new System.Drawing.Point(767, 151);
            this.btnIngresoCliente.Name = "btnIngresoCliente";
            this.btnIngresoCliente.Size = new System.Drawing.Size(201, 46);
            this.btnIngresoCliente.TabIndex = 90;
            this.btnIngresoCliente.Text = "Nuevo Cliente";
            this.btnIngresoCliente.UseVisualStyleBackColor = false;
            this.btnIngresoCliente.Click += new System.EventHandler(this.btnIngresoCliente_Click);
            // 
            // btnIngresoPlaca
            // 
            this.btnIngresoPlaca.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnIngresoPlaca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIngresoPlaca.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngresoPlaca.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnIngresoPlaca.Location = new System.Drawing.Point(767, 99);
            this.btnIngresoPlaca.Name = "btnIngresoPlaca";
            this.btnIngresoPlaca.Size = new System.Drawing.Size(201, 46);
            this.btnIngresoPlaca.TabIndex = 88;
            this.btnIngresoPlaca.Text = "Placa Nueva";
            this.btnIngresoPlaca.UseVisualStyleBackColor = false;
            this.btnIngresoPlaca.Click += new System.EventHandler(this.btnIngresoPlaca_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(41, 438);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1377, 404);
            this.dataGridView1.TabIndex = 87;
            // 
            // labelTema
            // 
            this.labelTema.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTema.BackColor = System.Drawing.Color.Red;
            this.labelTema.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTema.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelTema.Location = new System.Drawing.Point(-4, 0);
            this.labelTema.Name = "labelTema";
            this.labelTema.Size = new System.Drawing.Size(1384, 42);
            this.labelTema.TabIndex = 82;
            this.labelTema.Text = "Herramienta Rapida - Parqueo";
            this.labelTema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SeaGreen;
            this.panel2.Controls.Add(this.textBox6);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.textBox7);
            this.panel2.Controls.Add(this.textBox4);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.textBox5);
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtnomcli);
            this.panel2.Controls.Add(this.txtinvnum);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.plagascli);
            this.panel2.Controls.Add(this.txtplacas);
            this.panel2.Controls.Add(this.labelTema);
            this.panel2.Location = new System.Drawing.Point(38, 209);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1380, 155);
            this.panel2.TabIndex = 89;
            // 
            // btnIngreso
            // 
            this.btnIngreso.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnIngreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIngreso.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngreso.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnIngreso.Location = new System.Drawing.Point(1217, 101);
            this.btnIngreso.Name = "btnIngreso";
            this.btnIngreso.Size = new System.Drawing.Size(201, 44);
            this.btnIngreso.TabIndex = 92;
            this.btnIngreso.Text = "Ingreso";
            this.btnIngreso.UseVisualStyleBackColor = false;
            // 
            // btnSalida
            // 
            this.btnSalida.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSalida.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalida.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalida.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnSalida.Location = new System.Drawing.Point(1217, 152);
            this.btnSalida.Name = "btnSalida";
            this.btnSalida.Size = new System.Drawing.Size(201, 46);
            this.btnSalida.TabIndex = 93;
            this.btnSalida.Text = "Salida";
            this.btnSalida.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(391, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(346, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 94;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.Red;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(34, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 41);
            this.label1.TabIndex = 85;
            this.label1.Text = "Hora:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHora
            // 
            this.lblHora.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora.BackColor = System.Drawing.Color.Navy;
            this.lblHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblHora.Location = new System.Drawing.Point(151, 99);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(207, 41);
            this.lblHora.TabIndex = 95;
            this.lblHora.Text = "Hora:";
            this.lblHora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.Red;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(34, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 42);
            this.label2.TabIndex = 96;
            this.label2.Text = "Fecha:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFecha
            // 
            this.lblFecha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFecha.BackColor = System.Drawing.Color.Navy;
            this.lblFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblFecha.Location = new System.Drawing.Point(151, 151);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(207, 42);
            this.lblFecha.TabIndex = 97;
            this.lblFecha.Text = "Hora:";
            this.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // horafecha
            // 
            this.horafecha.Enabled = true;
            this.horafecha.Tick += new System.EventHandler(this.horafecha_Tick);
            // 
            // plagascli
            // 
            this.plagascli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plagascli.ForeColor = System.Drawing.Color.White;
            this.plagascli.Location = new System.Drawing.Point(40, 54);
            this.plagascli.Name = "plagascli";
            this.plagascli.Size = new System.Drawing.Size(55, 31);
            this.plagascli.TabIndex = 100;
            this.plagascli.Text = "Placas";
            this.plagascli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtplacas
            // 
            this.txtplacas.Location = new System.Drawing.Point(117, 63);
            this.txtplacas.Name = "txtplacas";
            this.txtplacas.Size = new System.Drawing.Size(217, 22);
            this.txtplacas.TabIndex = 101;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 31);
            this.label3.TabIndex = 102;
            this.label3.Text = "N°Inventario";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtinvnum
            // 
            this.txtinvnum.Location = new System.Drawing.Point(117, 106);
            this.txtinvnum.Name = "txtinvnum";
            this.txtinvnum.Size = new System.Drawing.Size(217, 22);
            this.txtinvnum.TabIndex = 103;
            // 
            // btnBuscarPlaca
            // 
            this.btnBuscarPlaca.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBuscarPlaca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarPlaca.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarPlaca.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBuscarPlaca.Location = new System.Drawing.Point(990, 151);
            this.btnBuscarPlaca.Name = "btnBuscarPlaca";
            this.btnBuscarPlaca.Size = new System.Drawing.Size(201, 46);
            this.btnBuscarPlaca.TabIndex = 98;
            this.btnBuscarPlaca.Text = "Buscar Placa";
            this.btnBuscarPlaca.UseVisualStyleBackColor = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(458, 106);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(217, 22);
            this.textBox2.TabIndex = 107;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(344, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 31);
            this.label4.TabIndex = 106;
            this.label4.Text = "Telefono";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(353, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 31);
            this.label5.TabIndex = 104;
            this.label5.Text = "Nombre";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtnomcli
            // 
            this.txtnomcli.Location = new System.Drawing.Point(458, 63);
            this.txtnomcli.Name = "txtnomcli";
            this.txtnomcli.Size = new System.Drawing.Size(217, 22);
            this.txtnomcli.TabIndex = 105;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(807, 106);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(217, 22);
            this.textBox4.TabIndex = 111;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(693, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 31);
            this.label6.TabIndex = 110;
            this.label6.Text = "F.Final";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(730, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 31);
            this.label7.TabIndex = 108;
            this.label7.Text = "F.Inicio";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(807, 63);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(217, 22);
            this.textBox5.TabIndex = 109;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(1145, 106);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(217, 22);
            this.textBox6.TabIndex = 115;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(1031, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 31);
            this.label8.TabIndex = 114;
            this.label8.Text = "Tiempo";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(1068, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 31);
            this.label9.TabIndex = 112;
            this.label9.Text = "Monto";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(1145, 63);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(217, 22);
            this.textBox7.TabIndex = 113;
            // 
            // btnReimprimirTicket
            // 
            this.btnReimprimirTicket.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnReimprimirTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReimprimirTicket.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReimprimirTicket.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnReimprimirTicket.Location = new System.Drawing.Point(38, 370);
            this.btnReimprimirTicket.Name = "btnReimprimirTicket";
            this.btnReimprimirTicket.Size = new System.Drawing.Size(1380, 46);
            this.btnReimprimirTicket.TabIndex = 99;
            this.btnReimprimirTicket.Text = "Reimprimir Ticket";
            this.btnReimprimirTicket.UseVisualStyleBackColor = false;
            // 
            // parqueo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1449, 888);
            this.Controls.Add(this.btnReimprimirTicket);
            this.Controls.Add(this.btnBuscarPlaca);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblHora);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSalida);
            this.Controls.Add(this.btnIngreso);
            this.Controls.Add(this.btnClienteRegistrado);
            this.Controls.Add(this.btnIngresoCliente);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnIngresoPlaca);
            this.Controls.Add(this.dataGridView1);
            this.Name = "parqueo";
            this.Text = "Sistema de Parqueo";
            this.Load += new System.EventHandler(this.parqueo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClienteRegistrado;
        private System.Windows.Forms.Button btnIngresoCliente;
        private System.Windows.Forms.Button btnIngresoPlaca;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label labelTema;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnIngreso;
        private System.Windows.Forms.Button btnSalida;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Timer horafecha;
        private System.Windows.Forms.Label plagascli;
        private System.Windows.Forms.TextBox txtplacas;
        private System.Windows.Forms.TextBox txtinvnum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtnomcli;
        private System.Windows.Forms.Button btnBuscarPlaca;
        private System.Windows.Forms.Button btnReimprimirTicket;
    }
}