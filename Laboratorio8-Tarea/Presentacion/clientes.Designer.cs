﻿namespace Presentacion
{
    partial class clientes
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.estadocli = new System.Windows.Forms.Label();
            this.tipotarifacli = new System.Windows.Forms.Label();
            this.telefonocli = new System.Windows.Forms.Label();
            this.txttelefonocli = new System.Windows.Forms.TextBox();
            this.nombrecli = new System.Windows.Forms.Label();
            this.txtnombrecli = new System.Windows.Forms.TextBox();
            this.codigocli = new System.Windows.Forms.Label();
            this.txtcodigocli = new System.Windows.Forms.TextBox();
            this.labelTema = new System.Windows.Forms.Label();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.txttipotarifacli = new System.Windows.Forms.ListBox();
            this.txtestadocli = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(529, 153);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(776, 553);
            this.dataGridView1.TabIndex = 76;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnGuardar.Location = new System.Drawing.Point(529, 99);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(219, 34);
            this.btnGuardar.TabIndex = 77;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SeaGreen;
            this.panel2.Controls.Add(this.txtestadocli);
            this.panel2.Controls.Add(this.txttipotarifacli);
            this.panel2.Controls.Add(this.estadocli);
            this.panel2.Controls.Add(this.tipotarifacli);
            this.panel2.Controls.Add(this.telefonocli);
            this.panel2.Controls.Add(this.txttelefonocli);
            this.panel2.Controls.Add(this.nombrecli);
            this.panel2.Controls.Add(this.txtnombrecli);
            this.panel2.Controls.Add(this.codigocli);
            this.panel2.Controls.Add(this.txtcodigocli);
            this.panel2.Controls.Add(this.labelTema);
            this.panel2.Location = new System.Drawing.Point(28, 99);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(425, 607);
            this.panel2.TabIndex = 79;
            // 
            // estadocli
            // 
            this.estadocli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.estadocli.ForeColor = System.Drawing.Color.White;
            this.estadocli.Location = new System.Drawing.Point(25, 235);
            this.estadocli.Name = "estadocli";
            this.estadocli.Size = new System.Drawing.Size(82, 19);
            this.estadocli.TabIndex = 91;
            this.estadocli.Text = "Estado";
            this.estadocli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tipotarifacli
            // 
            this.tipotarifacli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tipotarifacli.ForeColor = System.Drawing.Color.White;
            this.tipotarifacli.Location = new System.Drawing.Point(25, 193);
            this.tipotarifacli.Name = "tipotarifacli";
            this.tipotarifacli.Size = new System.Drawing.Size(82, 19);
            this.tipotarifacli.TabIndex = 89;
            this.tipotarifacli.Text = "Tipo Fact";
            this.tipotarifacli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // telefonocli
            // 
            this.telefonocli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.telefonocli.ForeColor = System.Drawing.Color.White;
            this.telefonocli.Location = new System.Drawing.Point(25, 153);
            this.telefonocli.Name = "telefonocli";
            this.telefonocli.Size = new System.Drawing.Size(82, 19);
            this.telefonocli.TabIndex = 87;
            this.telefonocli.Text = "Telefono";
            this.telefonocli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txttelefonocli
            // 
            this.txttelefonocli.Location = new System.Drawing.Point(122, 150);
            this.txttelefonocli.Name = "txttelefonocli";
            this.txttelefonocli.Size = new System.Drawing.Size(266, 22);
            this.txttelefonocli.TabIndex = 88;
            // 
            // nombrecli
            // 
            this.nombrecli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nombrecli.ForeColor = System.Drawing.Color.White;
            this.nombrecli.Location = new System.Drawing.Point(25, 111);
            this.nombrecli.Name = "nombrecli";
            this.nombrecli.Size = new System.Drawing.Size(82, 19);
            this.nombrecli.TabIndex = 85;
            this.nombrecli.Text = "Nombre";
            this.nombrecli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtnombrecli
            // 
            this.txtnombrecli.Location = new System.Drawing.Point(122, 108);
            this.txtnombrecli.Name = "txtnombrecli";
            this.txtnombrecli.Size = new System.Drawing.Size(266, 22);
            this.txtnombrecli.TabIndex = 86;
            // 
            // codigocli
            // 
            this.codigocli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.codigocli.ForeColor = System.Drawing.Color.White;
            this.codigocli.Location = new System.Drawing.Point(25, 70);
            this.codigocli.Name = "codigocli";
            this.codigocli.Size = new System.Drawing.Size(82, 19);
            this.codigocli.TabIndex = 83;
            this.codigocli.Text = "Codigo";
            this.codigocli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtcodigocli
            // 
            this.txtcodigocli.Location = new System.Drawing.Point(122, 67);
            this.txtcodigocli.Name = "txtcodigocli";
            this.txtcodigocli.Size = new System.Drawing.Size(266, 22);
            this.txtcodigocli.TabIndex = 84;
            // 
            // labelTema
            // 
            this.labelTema.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTema.BackColor = System.Drawing.Color.Red;
            this.labelTema.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTema.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelTema.Location = new System.Drawing.Point(-53, 0);
            this.labelTema.Name = "labelTema";
            this.labelTema.Size = new System.Drawing.Size(478, 30);
            this.labelTema.TabIndex = 82;
            this.labelTema.Text = "Herramienta Rapida - Clientes";
            this.labelTema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditar.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnEditar.Location = new System.Drawing.Point(815, 99);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(219, 34);
            this.btnEditar.TabIndex = 80;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnEliminar.Location = new System.Drawing.Point(1086, 99);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(219, 34);
            this.btnEliminar.TabIndex = 81;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // txttipotarifacli
            // 
            this.txttipotarifacli.FormattingEnabled = true;
            this.txttipotarifacli.ItemHeight = 16;
            this.txttipotarifacli.Items.AddRange(new object[] {
            "",
            "F",
            "B"});
            this.txttipotarifacli.Location = new System.Drawing.Point(122, 192);
            this.txttipotarifacli.Name = "txttipotarifacli";
            this.txttipotarifacli.Size = new System.Drawing.Size(126, 20);
            this.txttipotarifacli.TabIndex = 93;
            // 
            // txtestadocli
            // 
            this.txtestadocli.FormattingEnabled = true;
            this.txtestadocli.ItemHeight = 16;
            this.txtestadocli.Items.AddRange(new object[] {
            "",
            "A",
            "D"});
            this.txtestadocli.Location = new System.Drawing.Point(122, 235);
            this.txtestadocli.Name = "txtestadocli";
            this.txtestadocli.Size = new System.Drawing.Size(126, 20);
            this.txtestadocli.TabIndex = 94;
            // 
            // clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1347, 731);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.dataGridView1);
            this.Name = "clientes";
            this.Text = "Mantenimiento Clientes";
            this.Load += new System.EventHandler(this.clientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label estadocli;
        private System.Windows.Forms.Label tipotarifacli;
        private System.Windows.Forms.Label telefonocli;
        private System.Windows.Forms.TextBox txttelefonocli;
        private System.Windows.Forms.Label nombrecli;
        private System.Windows.Forms.TextBox txtnombrecli;
        private System.Windows.Forms.Label codigocli;
        private System.Windows.Forms.TextBox txtcodigocli;
        private System.Windows.Forms.Label labelTema;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.ListBox txttipotarifacli;
        private System.Windows.Forms.ListBox txtestadocli;
    }
}

