﻿namespace Presentacion
{
    partial class placas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tipotarifacli = new System.Windows.Forms.Label();
            this.telefonocli = new System.Windows.Forms.Label();
            this.nombrecli = new System.Windows.Forms.Label();
            this.txtcaractplacas = new System.Windows.Forms.TextBox();
            this.plagascli = new System.Windows.Forms.Label();
            this.txtplacas = new System.Windows.Forms.TextBox();
            this.labelTema = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtestadoplacas = new System.Windows.Forms.ListBox();
            this.txtnombrecli = new System.Windows.Forms.ListBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnEliminar.Location = new System.Drawing.Point(1084, 92);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(219, 34);
            this.btnEliminar.TabIndex = 86;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditar.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnEditar.Location = new System.Drawing.Point(813, 92);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(219, 34);
            this.btnEditar.TabIndex = 85;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SeaGreen;
            this.panel2.Controls.Add(this.txtnombrecli);
            this.panel2.Controls.Add(this.txtestadoplacas);
            this.panel2.Controls.Add(this.tipotarifacli);
            this.panel2.Controls.Add(this.telefonocli);
            this.panel2.Controls.Add(this.nombrecli);
            this.panel2.Controls.Add(this.txtcaractplacas);
            this.panel2.Controls.Add(this.plagascli);
            this.panel2.Controls.Add(this.txtplacas);
            this.panel2.Controls.Add(this.labelTema);
            this.panel2.Location = new System.Drawing.Point(26, 92);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(425, 607);
            this.panel2.TabIndex = 84;
            // 
            // tipotarifacli
            // 
            this.tipotarifacli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tipotarifacli.ForeColor = System.Drawing.Color.White;
            this.tipotarifacli.Location = new System.Drawing.Point(33, 190);
            this.tipotarifacli.Name = "tipotarifacli";
            this.tipotarifacli.Size = new System.Drawing.Size(82, 19);
            this.tipotarifacli.TabIndex = 89;
            this.tipotarifacli.Text = "Estado";
            this.tipotarifacli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // telefonocli
            // 
            this.telefonocli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.telefonocli.ForeColor = System.Drawing.Color.White;
            this.telefonocli.Location = new System.Drawing.Point(33, 150);
            this.telefonocli.Name = "telefonocli";
            this.telefonocli.Size = new System.Drawing.Size(82, 19);
            this.telefonocli.TabIndex = 87;
            this.telefonocli.Text = "Clientes";
            this.telefonocli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nombrecli
            // 
            this.nombrecli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nombrecli.ForeColor = System.Drawing.Color.White;
            this.nombrecli.Location = new System.Drawing.Point(11, 108);
            this.nombrecli.Name = "nombrecli";
            this.nombrecli.Size = new System.Drawing.Size(104, 19);
            this.nombrecli.TabIndex = 85;
            this.nombrecli.Text = "Caracteristicas";
            this.nombrecli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtcaractplacas
            // 
            this.txtcaractplacas.Location = new System.Drawing.Point(130, 105);
            this.txtcaractplacas.Name = "txtcaractplacas";
            this.txtcaractplacas.Size = new System.Drawing.Size(266, 22);
            this.txtcaractplacas.TabIndex = 86;
            // 
            // plagascli
            // 
            this.plagascli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plagascli.ForeColor = System.Drawing.Color.White;
            this.plagascli.Location = new System.Drawing.Point(33, 67);
            this.plagascli.Name = "plagascli";
            this.plagascli.Size = new System.Drawing.Size(82, 19);
            this.plagascli.TabIndex = 83;
            this.plagascli.Text = "Placas";
            this.plagascli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtplacas
            // 
            this.txtplacas.Location = new System.Drawing.Point(130, 64);
            this.txtplacas.Name = "txtplacas";
            this.txtplacas.Size = new System.Drawing.Size(266, 22);
            this.txtplacas.TabIndex = 84;
            // 
            // labelTema
            // 
            this.labelTema.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTema.BackColor = System.Drawing.Color.Red;
            this.labelTema.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTema.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelTema.Location = new System.Drawing.Point(-53, 0);
            this.labelTema.Name = "labelTema";
            this.labelTema.Size = new System.Drawing.Size(478, 30);
            this.labelTema.TabIndex = 82;
            this.labelTema.Text = "Herramienta Rapida - Placas";
            this.labelTema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnGuardar.Location = new System.Drawing.Point(527, 92);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(219, 34);
            this.btnGuardar.TabIndex = 83;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(527, 146);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(776, 553);
            this.dataGridView1.TabIndex = 82;
            // 
            // txtestadoplacas
            // 
            this.txtestadoplacas.FormattingEnabled = true;
            this.txtestadoplacas.ItemHeight = 16;
            this.txtestadoplacas.Items.AddRange(new object[] {
            "",
            "A",
            "D"});
            this.txtestadoplacas.Location = new System.Drawing.Point(130, 190);
            this.txtestadoplacas.Name = "txtestadoplacas";
            this.txtestadoplacas.Size = new System.Drawing.Size(126, 20);
            this.txtestadoplacas.TabIndex = 95;
            // 
            // txtnombrecli
            // 
            this.txtnombrecli.FormattingEnabled = true;
            this.txtnombrecli.ItemHeight = 16;
            this.txtnombrecli.Items.AddRange(new object[] {
            ""});
            this.txtnombrecli.Location = new System.Drawing.Point(130, 150);
            this.txtnombrecli.Name = "txtnombrecli";
            this.txtnombrecli.ScrollAlwaysVisible = true;
            this.txtnombrecli.Size = new System.Drawing.Size(266, 20);
            this.txtnombrecli.TabIndex = 96;
            this.txtnombrecli.SelectedIndexChanged += new System.EventHandler(this.txtnombrecli_SelectedIndexChanged);
            // 
            // placas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1347, 731);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.dataGridView1);
            this.Name = "placas";
            this.Text = "Mantenimiento Placas";
            this.Load += new System.EventHandler(this.placas_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label tipotarifacli;
        private System.Windows.Forms.Label telefonocli;
        private System.Windows.Forms.Label nombrecli;
        private System.Windows.Forms.TextBox txtcaractplacas;
        private System.Windows.Forms.Label labelTema;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label plagascli;
        private System.Windows.Forms.TextBox txtplacas;
        private System.Windows.Forms.ListBox txtestadoplacas;
        public System.Windows.Forms.ListBox txtnombrecli;
    }
}