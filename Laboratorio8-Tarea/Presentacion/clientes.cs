﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Diseño de Form
using MaterialSkin;

// Usar capa negocio
using Negocio;

namespace Presentacion
{
    public partial class clientes : MaterialSkin.Controls.MaterialForm
    {

        private N_Clientes objetoCN = new N_Clientes();
        private string PK_clientes = null;  // primary key
        private bool Editar = false;

        public clientes()
        {
            InitializeComponent();
        }

        //Limpieza de Datos
        private void limpiarForm()
        {

            txtcodigocli.Clear();
            txtnombrecli.Clear();
            txttelefonocli.Clear();
          //  txttipotarifacli.Clear();
          //  txtestadocli.Clear();
        
            }



        // Listado Inicio y Actulizado
        private void MostrarCliente()
        {

            N_Clientes objeto = new N_Clientes();
            dataGridView1.DataSource = objeto.MostrarCliente();
        }

        // Listar Cliente
        private void clientes_Load(object sender, EventArgs e)
        {
            MostrarCliente();
        }
        /*
         */

        // Boton Insertar y Guardars  
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
            //INSERTAR
            if (Editar == false)
            {
                try
                {
                    objetoCN.InsertarCliente(txtcodigocli.Text ,txtnombrecli.Text, txttelefonocli.Text, txttipotarifacli.Text, txtestadocli.Text);
                    MessageBox.Show("Se inserto correctamente");
                    MostrarCliente();
                    limpiarForm();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se pudo insertar los datos por: " + ex);
                }
            }
            //EDITAR
            if (Editar == true)
            {

                try
                {
                    objetoCN.EditarCliente(txtcodigocli.Text,txtnombrecli.Text, txttelefonocli.Text, txttipotarifacli.Text, txtestadocli.Text, PK_clientes);
                    MessageBox.Show("Se edito correctamente");
                    MostrarCliente();
                    limpiarForm();
                    Editar = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se pudo editar los datos por: " + ex);
                }
            }

            
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Editar = true;
                txtcodigocli.Text = dataGridView1.CurrentRow.Cells["codcli"].Value.ToString();
                txtnombrecli.Text = dataGridView1.CurrentRow.Cells["nomcli"].Value.ToString();
                txttelefonocli.Text = dataGridView1.CurrentRow.Cells["telcli"].Value.ToString();
                txttipotarifacli.Text = dataGridView1.CurrentRow.Cells["tiptar"].Value.ToString();
                txtestadocli.Text = dataGridView1.CurrentRow.Cells["estado"].Value.ToString();
                PK_clientes = dataGridView1.CurrentRow.Cells["PK_clientes"].Value.ToString();


            }
            else
                MessageBox.Show("Seleccione una fila por favor");
            
        }


        private void btnEliminar_Click(object sender, EventArgs e)
        {
           
            if (dataGridView1.SelectedRows.Count > 0)
            {
                PK_clientes = dataGridView1.CurrentRow.Cells["PK_clientes"].Value.ToString();
                objetoCN.EliminarCliente(PK_clientes);
                MessageBox.Show("Eliminado Cliente correctamente");
                MostrarCliente();
            }
            else
                MessageBox.Show("Seleccione una fila por favor");
          
        }


    }
}