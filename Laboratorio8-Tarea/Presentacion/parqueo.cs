﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño de Form
using MaterialSkin;

// Usar capa negocio
using Negocio;


namespace Presentacion
{
    public partial class parqueo : MaterialSkin.Controls.MaterialForm
    {
        public parqueo()
        {
            InitializeComponent();
        }


        // Listado Inicio y Actulizado
        private void MostrarRegistro()
        {

            N_Registro objeto = new N_Registro();
            dataGridView1.DataSource = objeto.MostrarRegistro();
        }


        // listado de  registro de parqueo
        private void parqueo_Load(object sender, EventArgs e)
        {
            MostrarRegistro();
        }
        // Ingreso a Mantenimiento Placa
        private void btnIngresoPlaca_Click(object sender, EventArgs e)
        {
            placas ingresoplacas = new placas();
            ingresoplacas.Show();
            //  this.Close();
        }
        // Ingreso a Mantenimiento Cliente
        private void btnIngresoCliente_Click(object sender, EventArgs e)
        {
            clientes ingresoplacas = new clientes();
            ingresoplacas.Show();
          //  this.Close();
        }

        private void btnReimprimirTicket_Click(object sender, EventArgs e)
        {

        }

        // MOstrar hora fecha
        private void horafecha_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToLongTimeString();
            lblFecha.Text = DateTime.Now.ToShortDateString();

            //  lblFecha.Text = DateTime.Now.ToString("dddd:MMMM:yyy");

        }
    }
}
