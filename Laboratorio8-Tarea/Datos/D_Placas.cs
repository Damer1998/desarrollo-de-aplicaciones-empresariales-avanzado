﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// SQL
using System.Data.SqlClient;
using System.Data;


namespace Datos
{
    public class D_Placas
    {


        // Listar Placas 
        private CD_Conexion conexion = new CD_Conexion();

        SqlDataReader leer;
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();


        // Mostrar  Plcas      comando.CommandText = "MostrarVehiculos";
        public DataTable Mostrar()
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "MostrarVehiculos";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;

        }


        // Mostrar  Plcas- Clientes      comando.CommandText = "MostrarVehiculos";
        public DataTable MostrarClientesRegistro()
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "MostrarClientesRegistro";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;

        }




        // Insertar datos para Placa

        public void Insertar( string placa, string caracteristicas, string nomcli, string estado)
        {
            //PROCEDIMNIENTO

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "InsetarVehiculos";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@placa", placa);
            comando.Parameters.AddWithValue("@caracteristicas", caracteristicas);
            comando.Parameters.AddWithValue("@nomcli", nomcli);
            comando.Parameters.AddWithValue("@estado", estado);
       

            comando.ExecuteNonQuery();

            comando.Parameters.Clear();

        }

        // Editar Placa
        public void Editar(string placa, string caracteristicas, string nomcli, string estado, int PK_vehiculos)
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EditarVehiculos";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@placa", placa);
            comando.Parameters.AddWithValue("@caracteristicas", caracteristicas);
            comando.Parameters.AddWithValue("@nomcli", nomcli);
            comando.Parameters.AddWithValue("@estado", estado);
            comando.Parameters.AddWithValue("@PK_vehiculos", PK_vehiculos);
            comando.ExecuteNonQuery();

            comando.Parameters.Clear();
        }
        // Eliminar Placas

        public void Eliminar(int PK_vehiculos)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EliminarVehiculos";
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@PK_vehiculos", PK_vehiculos);

            comando.ExecuteNonQuery();

            comando.Parameters.Clear();
        }














    }
}
