﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// SQL
using System.Data.SqlClient;
using System.Data;



namespace Datos
{
    public class D_Registro
    {

        // Listar registro - Relacion a Placas y Clientes
        private CD_Conexion conexion = new CD_Conexion();

        SqlDataReader leer;
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();



        // Mostrar lista de registro llamado de sql server procedimientio MostrarRegistro
        public DataTable Mostrar()
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "MostrarRegistro";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;

        }



    }
}
