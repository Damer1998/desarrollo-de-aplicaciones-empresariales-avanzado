﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// SQL
using System.Data.SqlClient;
using System.Data;

namespace Datos
{
    public class D_Clientes
    {

        // Listar Clientes
        private CD_Conexion conexion = new CD_Conexion();

        SqlDataReader leer;
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();


        // Mostrar 
        public DataTable Mostrar()
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "MostrarClientes";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;

        }

 

        public void Insertar(int codcli, string nomcli, string telcli, string tiptar, string estado)
        {
            //PROCEDIMNIENTO

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "InsetarClientes";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@codcli", codcli);
            comando.Parameters.AddWithValue("@nomcli", nomcli);
            comando.Parameters.AddWithValue("@telcli", telcli);
            comando.Parameters.AddWithValue("@tiptar", tiptar);
            comando.Parameters.AddWithValue("@estado", estado);

            comando.ExecuteNonQuery();

            comando.Parameters.Clear();

        }
        /*  */
        public void Editar(int codcli, string nomcli, string telcli, string tiptar, string estado , int PK_clientes)
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EditarClientes";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@codcli", codcli);
            comando.Parameters.AddWithValue("@nomcli", nomcli);
            comando.Parameters.AddWithValue("@telcli", telcli);
            comando.Parameters.AddWithValue("@tiptar", tiptar);
            comando.Parameters.AddWithValue("@estado", estado);
            comando.Parameters.AddWithValue("@PK_clientes", PK_clientes);
            comando.ExecuteNonQuery();

            comando.Parameters.Clear();
        }
             

        public void Eliminar(int PK_clientes)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EliminarClientes";
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@PK_clientes", PK_clientes);

            comando.ExecuteNonQuery();

            comando.Parameters.Clear();
        }


  

    }
}
