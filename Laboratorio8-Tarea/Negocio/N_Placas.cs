﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Data;
using System.Data.SqlClient;
// Uso de CApa
using Datos;




namespace Negocio
{
   public class N_Placas
    {

        private D_Placas objetoPD = new D_Placas();

        public DataTable MostrarVehiculos()
        {

            DataTable tabla = new DataTable();
            tabla = objetoPD.Mostrar();
            return tabla;
        }

        // Listar clientes en list box
        public DataTable MostrarClientesRegistro()
        {

            DataTable tabla1 = new DataTable();
            tabla1 = objetoPD.MostrarClientesRegistro();
            return tabla1;
        }


        public void InsetarVehiculos(string placa, string caracteristicas, string nomcli, string estado)
        {
      
            objetoPD.Insertar( placa, caracteristicas, nomcli,estado);
            //  objetoCD.Insertar(nombre,desc,marca,Convert.ToDouble(precio),Convert.ToInt32(stock)); float
        }


        public void EditarVehiculos(string placa, string caracteristicas,  string nomcli ,string estado, string PK_vehiculos)
        {
            objetoPD.Editar(placa,caracteristicas,nomcli,estado, Convert.ToInt32(PK_vehiculos));
        }




        public void EliminarVehiculos(string PK_vehiculos)
        {

            objetoPD.Eliminar(Convert.ToInt32(PK_vehiculos));
        }







    }
}
