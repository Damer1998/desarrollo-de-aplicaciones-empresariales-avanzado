﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorio1_App01_V1_Interfaz
{
    public partial class CalculoBasico : Form
    {
        public CalculoBasico()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }





        private void label8_Click(object sender, EventArgs e)
        {

        }

        // 10 Primeros Numeros
        private void btnPrimo(object sender, EventArgs e)
        {
            int cont = 0;
            for (int i = 2; i <= 30; i++)
            {

                for (int j = 1; j <= i; j++)
                {
                    // Realiza comparacion
                    if (i % j == 0)
                    {
                        cont = cont + 1;
                    }
                }

                //si variable cont es mayor a dos, el numero es divisible en mas de dos numero osea no es primo
                if (cont <= 2)
                {
                  listboxPrimos.Items.Add(i);
                }

                //igualamos a cero para hacer otra evaluacion con otro numero
                cont = 0;


            }



        }



        // Listar para raiz

        private void btnRaiz(object sender, EventArgs e)
        {
       
                for (int i = 1; i <= 10; i++)
                {

                double resultadosRaiz = Math.Sqrt(i);
                listboxRaiz.Items.Add(resultadosRaiz);
              
                }
           
        }


        //Sesion de Operaciones Basicas
        private void sumarEjercutar(object sender, EventArgs e)
        {
            // Boton Sumar
              
            float respuestaSuma = 0;
            respuestaSuma = float.Parse(datoaA.Text) + float.Parse(datoB.Text);

            datorespuesta.Text = respuestaSuma.ToString();
            
        }


        private void restarEjercutar(object sender, EventArgs e)
        {
            // Boton Restar

            float respuestaRestar = 0;

            respuestaRestar = float.Parse(datoaA.Text) - float.Parse(datoB.Text);

            datorespuesta.Text = respuestaRestar.ToString();

        }

        private void dividirEjecutar(object sender, EventArgs e)
        {
            // Boton Dividir

            float respuestaDividir = 0;

            respuestaDividir = float.Parse(datoaA.Text) / float.Parse(datoB.Text);
            datorespuesta.Text = respuestaDividir.ToString();

        }

        private void multiplicarEjecutar(object sender, EventArgs e)
        {
            float respuestaMultiplicar = 0;

            respuestaMultiplicar = float.Parse(datoaA.Text) * float.Parse(datoB.Text);

            datorespuesta.Text = respuestaMultiplicar.ToString();
        }

        // Formatear Operaciones Basica
        private void limpiarEjecutar(object sender, EventArgs e)
        {

            datoaA.Clear();
            datoB.Clear();
            datorespuesta.Clear();
            
        }



        // Ingreso de Datos e Resultado
        private void datoaA_TextChanged(object sender, EventArgs e)
        {

        }

        private void datoB_TextChanged(object sender, EventArgs e)
        {

        }

        private void datorespuesta_Click(object sender, EventArgs e)
        {

        }

        private void datoC_TextChanged(object sender, EventArgs e)
        {

        }

        private void datoF_TextChanged(object sender, EventArgs e)
        {

        }

        private void datorespuesta_TextChanged(object sender, EventArgs e)
        {

        }

        private void respuestaF_TextChanged(object sender, EventArgs e)
        {

        }

        private void respuestaC_TextChanged(object sender, EventArgs e)
        {

        }

        // Convertir de Celsius a Farenheit
        private void CaFEjecutar(object sender, EventArgs e)
        {
            //Farenheit
            float respuestaCaF = 0;
            respuestaCaF = (float.Parse(datoC.Text) - 32) * 5 / 9;

            respuestaF.Text = respuestaCaF.ToString();

        }
        // Convertir de Farenheit a Celsius
        private void FaCEjecutar(object sender, EventArgs e)
        {
            //Celsius
            float respuestaFaC = 0;
            respuestaFaC = (float.Parse(datoF.Text) * 9 / 5) + 32;

            respuestaC.Text = respuestaFaC.ToString();
        }


        // Eliminar Temperatura
        private void FaCeliminar(object sender, EventArgs e)
        {
            datoC.Clear();
            respuestaF.Clear();
        }

        private void CaFeliminar(object sender, EventArgs e)
        {
            datoF.Clear();
            respuestaC.Clear();
        }


        private void btnEliminarPrimos_Click(object sender, EventArgs e)
        {
            listboxPrimos.Items.Remove(listboxPrimos.SelectedItem);
          
        }

        private void btnEliminarRaiz_Click(object sender, EventArgs e)
        {

            listboxRaiz.Items.Remove(listboxRaiz.SelectedItem);
          
        }



        private void txtPrimos_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
