﻿namespace Laboratorio1_App01_V1_Interfaz
{
    partial class CalculoBasico
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxBasicas = new System.Windows.Forms.GroupBox();
            this.datorespuesta = new System.Windows.Forms.TextBox();
            this.datoB = new System.Windows.Forms.TextBox();
            this.datoaA = new System.Windows.Forms.TextBox();
            this.btnDividir = new System.Windows.Forms.Button();
            this.btnMultiplicar = new System.Windows.Forms.Button();
            this.btnRestar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnSumar = new System.Windows.Forms.Button();
            this.temaHe = new System.Windows.Forms.Label();
            this.temaRe = new System.Windows.Forms.Label();
            this.temB = new System.Windows.Forms.Label();
            this.temA = new System.Windows.Forms.Label();
            this.groupBoxTemperaturas = new System.Windows.Forms.GroupBox();
            this.respuestaC = new System.Windows.Forms.TextBox();
            this.respuestaF = new System.Windows.Forms.TextBox();
            this.datoF = new System.Windows.Forms.TextBox();
            this.datoC = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.btnF = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.temaHe1 = new System.Windows.Forms.Label();
            this.temaF = new System.Windows.Forms.Label();
            this.temaC = new System.Windows.Forms.Label();
            this.groupBoxPrimeros = new System.Windows.Forms.GroupBox();
            this.btnListaR = new System.Windows.Forms.Button();
            this.btnListaP = new System.Windows.Forms.Button();
            this.temaRaiz = new System.Windows.Forms.Label();
            this.temaPrimos = new System.Windows.Forms.Label();
            this.listboxPrimos = new System.Windows.Forms.ListBox();
            this.listboxRaiz = new System.Windows.Forms.ListBox();
            this.btnEliminarPrimos = new System.Windows.Forms.Button();
            this.btnEliminarRaiz = new System.Windows.Forms.Button();
            this.groupBoxBasicas.SuspendLayout();
            this.groupBoxTemperaturas.SuspendLayout();
            this.groupBoxPrimeros.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxBasicas
            // 
            this.groupBoxBasicas.BackColor = System.Drawing.Color.PaleTurquoise;
            this.groupBoxBasicas.Controls.Add(this.datorespuesta);
            this.groupBoxBasicas.Controls.Add(this.datoB);
            this.groupBoxBasicas.Controls.Add(this.datoaA);
            this.groupBoxBasicas.Controls.Add(this.btnDividir);
            this.groupBoxBasicas.Controls.Add(this.btnMultiplicar);
            this.groupBoxBasicas.Controls.Add(this.btnRestar);
            this.groupBoxBasicas.Controls.Add(this.btnLimpiar);
            this.groupBoxBasicas.Controls.Add(this.btnSumar);
            this.groupBoxBasicas.Controls.Add(this.temaHe);
            this.groupBoxBasicas.Controls.Add(this.temaRe);
            this.groupBoxBasicas.Controls.Add(this.temB);
            this.groupBoxBasicas.Controls.Add(this.temA);
            this.groupBoxBasicas.Location = new System.Drawing.Point(12, 12);
            this.groupBoxBasicas.Name = "groupBoxBasicas";
            this.groupBoxBasicas.Size = new System.Drawing.Size(249, 523);
            this.groupBoxBasicas.TabIndex = 0;
            this.groupBoxBasicas.TabStop = false;
            this.groupBoxBasicas.Text = "Operaciones Basicas";
            this.groupBoxBasicas.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // datorespuesta
            // 
            this.datorespuesta.BackColor = System.Drawing.Color.SandyBrown;
            this.datorespuesta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datorespuesta.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.datorespuesta.Location = new System.Drawing.Point(3, 264);
            this.datorespuesta.Name = "datorespuesta";
            this.datorespuesta.Size = new System.Drawing.Size(237, 27);
            this.datorespuesta.TabIndex = 12;
            this.datorespuesta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.datorespuesta.TextChanged += new System.EventHandler(this.datorespuesta_TextChanged);
            // 
            // datoB
            // 
            this.datoB.BackColor = System.Drawing.Color.Azure;
            this.datoB.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datoB.Location = new System.Drawing.Point(6, 171);
            this.datoB.Name = "datoB";
            this.datoB.Size = new System.Drawing.Size(237, 27);
            this.datoB.TabIndex = 10;
            this.datoB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.datoB.TextChanged += new System.EventHandler(this.datoB_TextChanged);
            // 
            // datoaA
            // 
            this.datoaA.BackColor = System.Drawing.Color.Azure;
            this.datoaA.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datoaA.Location = new System.Drawing.Point(6, 85);
            this.datoaA.Name = "datoaA";
            this.datoaA.Size = new System.Drawing.Size(237, 27);
            this.datoaA.TabIndex = 9;
            this.datoaA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.datoaA.TextChanged += new System.EventHandler(this.datoaA_TextChanged);
            // 
            // btnDividir
            // 
            this.btnDividir.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnDividir.Location = new System.Drawing.Point(136, 396);
            this.btnDividir.Name = "btnDividir";
            this.btnDividir.Size = new System.Drawing.Size(107, 38);
            this.btnDividir.TabIndex = 8;
            this.btnDividir.Text = "Dividir";
            this.btnDividir.UseVisualStyleBackColor = false;
            this.btnDividir.Click += new System.EventHandler(this.dividirEjecutar);
            // 
            // btnMultiplicar
            // 
            this.btnMultiplicar.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnMultiplicar.Location = new System.Drawing.Point(136, 440);
            this.btnMultiplicar.Name = "btnMultiplicar";
            this.btnMultiplicar.Size = new System.Drawing.Size(107, 38);
            this.btnMultiplicar.TabIndex = 7;
            this.btnMultiplicar.Text = "Multiplicar";
            this.btnMultiplicar.UseVisualStyleBackColor = false;
            this.btnMultiplicar.Click += new System.EventHandler(this.multiplicarEjecutar);
            // 
            // btnRestar
            // 
            this.btnRestar.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnRestar.Location = new System.Drawing.Point(3, 440);
            this.btnRestar.Name = "btnRestar";
            this.btnRestar.Size = new System.Drawing.Size(107, 38);
            this.btnRestar.TabIndex = 6;
            this.btnRestar.Text = "Restar";
            this.btnRestar.UseVisualStyleBackColor = false;
            this.btnRestar.Click += new System.EventHandler(this.restarEjercutar);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackColor = System.Drawing.Color.Brown;
            this.btnLimpiar.Location = new System.Drawing.Point(0, 480);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(237, 38);
            this.btnLimpiar.TabIndex = 5;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = false;
            this.btnLimpiar.Click += new System.EventHandler(this.limpiarEjecutar);
            // 
            // btnSumar
            // 
            this.btnSumar.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnSumar.Location = new System.Drawing.Point(3, 396);
            this.btnSumar.Name = "btnSumar";
            this.btnSumar.Size = new System.Drawing.Size(107, 38);
            this.btnSumar.TabIndex = 4;
            this.btnSumar.Text = "Sumar";
            this.btnSumar.UseVisualStyleBackColor = false;
            this.btnSumar.Click += new System.EventHandler(this.sumarEjercutar);
            // 
            // temaHe
            // 
            this.temaHe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temaHe.BackColor = System.Drawing.Color.GreenYellow;
            this.temaHe.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temaHe.Location = new System.Drawing.Point(3, 343);
            this.temaHe.Margin = new System.Windows.Forms.Padding(0);
            this.temaHe.Name = "temaHe";
            this.temaHe.Size = new System.Drawing.Size(249, 39);
            this.temaHe.TabIndex = 3;
            this.temaHe.Text = "Herramientas";
            this.temaHe.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.temaHe.Click += new System.EventHandler(this.label4_Click);
            // 
            // temaRe
            // 
            this.temaRe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temaRe.BackColor = System.Drawing.Color.GreenYellow;
            this.temaRe.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temaRe.Location = new System.Drawing.Point(0, 212);
            this.temaRe.Margin = new System.Windows.Forms.Padding(0);
            this.temaRe.Name = "temaRe";
            this.temaRe.Size = new System.Drawing.Size(249, 38);
            this.temaRe.TabIndex = 2;
            this.temaRe.Text = "Resultado";
            this.temaRe.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.temaRe.Click += new System.EventHandler(this.label3_Click);
            // 
            // temB
            // 
            this.temB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temB.BackColor = System.Drawing.Color.GreenYellow;
            this.temB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temB.Location = new System.Drawing.Point(0, 121);
            this.temB.Margin = new System.Windows.Forms.Padding(0);
            this.temB.Name = "temB";
            this.temB.Size = new System.Drawing.Size(249, 37);
            this.temB.TabIndex = 1;
            this.temB.Text = "Ingresar numero B";
            this.temB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.temB.Click += new System.EventHandler(this.label2_Click);
            // 
            // temA
            // 
            this.temA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temA.BackColor = System.Drawing.Color.GreenYellow;
            this.temA.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temA.Location = new System.Drawing.Point(0, 34);
            this.temA.Margin = new System.Windows.Forms.Padding(0);
            this.temA.Name = "temA";
            this.temA.Size = new System.Drawing.Size(249, 38);
            this.temA.TabIndex = 0;
            this.temA.Text = "Ingresar numero A";
            this.temA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.temA.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBoxTemperaturas
            // 
            this.groupBoxTemperaturas.BackColor = System.Drawing.Color.NavajoWhite;
            this.groupBoxTemperaturas.Controls.Add(this.respuestaC);
            this.groupBoxTemperaturas.Controls.Add(this.respuestaF);
            this.groupBoxTemperaturas.Controls.Add(this.datoF);
            this.groupBoxTemperaturas.Controls.Add(this.datoC);
            this.groupBoxTemperaturas.Controls.Add(this.button8);
            this.groupBoxTemperaturas.Controls.Add(this.button7);
            this.groupBoxTemperaturas.Controls.Add(this.btnF);
            this.groupBoxTemperaturas.Controls.Add(this.btnC);
            this.groupBoxTemperaturas.Controls.Add(this.temaHe1);
            this.groupBoxTemperaturas.Controls.Add(this.temaF);
            this.groupBoxTemperaturas.Controls.Add(this.temaC);
            this.groupBoxTemperaturas.Location = new System.Drawing.Point(278, 12);
            this.groupBoxTemperaturas.Name = "groupBoxTemperaturas";
            this.groupBoxTemperaturas.Size = new System.Drawing.Size(258, 523);
            this.groupBoxTemperaturas.TabIndex = 1;
            this.groupBoxTemperaturas.TabStop = false;
            this.groupBoxTemperaturas.Text = "Temperaturas";
            // 
            // respuestaC
            // 
            this.respuestaC.BackColor = System.Drawing.Color.PaleTurquoise;
            this.respuestaC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.respuestaC.ForeColor = System.Drawing.SystemColors.ControlText;
            this.respuestaC.Location = new System.Drawing.Point(10, 281);
            this.respuestaC.Name = "respuestaC";
            this.respuestaC.Size = new System.Drawing.Size(242, 27);
            this.respuestaC.TabIndex = 14;
            this.respuestaC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.respuestaC.TextChanged += new System.EventHandler(this.respuestaC_TextChanged);
            // 
            // respuestaF
            // 
            this.respuestaF.BackColor = System.Drawing.Color.PaleTurquoise;
            this.respuestaF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.respuestaF.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.respuestaF.Location = new System.Drawing.Point(9, 125);
            this.respuestaF.Name = "respuestaF";
            this.respuestaF.Size = new System.Drawing.Size(242, 27);
            this.respuestaF.TabIndex = 13;
            this.respuestaF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.respuestaF.TextChanged += new System.EventHandler(this.respuestaF_TextChanged);
            // 
            // datoF
            // 
            this.datoF.BackColor = System.Drawing.Color.Azure;
            this.datoF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datoF.Location = new System.Drawing.Point(8, 248);
            this.datoF.Name = "datoF";
            this.datoF.Size = new System.Drawing.Size(243, 27);
            this.datoF.TabIndex = 13;
            this.datoF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.datoF.TextChanged += new System.EventHandler(this.datoF_TextChanged);
            // 
            // datoC
            // 
            this.datoC.BackColor = System.Drawing.Color.Azure;
            this.datoC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datoC.Location = new System.Drawing.Point(9, 85);
            this.datoC.Name = "datoC";
            this.datoC.Size = new System.Drawing.Size(243, 27);
            this.datoC.TabIndex = 11;
            this.datoC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.datoC.TextChanged += new System.EventHandler(this.datoC_TextChanged);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Brown;
            this.button8.BackgroundImage = global::Laboratorio1_App01_V1_Interfaz.Properties.Resources.basura1;
            this.button8.Location = new System.Drawing.Point(181, 427);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(42, 48);
            this.button8.TabIndex = 12;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.CaFeliminar);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Brown;
            this.button7.BackgroundImage = global::Laboratorio1_App01_V1_Interfaz.Properties.Resources.basura1;
            this.button7.Location = new System.Drawing.Point(181, 375);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(42, 46);
            this.button7.TabIndex = 9;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.FaCeliminar);
            // 
            // btnF
            // 
            this.btnF.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnF.Location = new System.Drawing.Point(9, 429);
            this.btnF.Name = "btnF";
            this.btnF.Size = new System.Drawing.Size(166, 46);
            this.btnF.TabIndex = 11;
            this.btnF.Text = "C° a F°   ";
            this.btnF.UseVisualStyleBackColor = false;
            this.btnF.Click += new System.EventHandler(this.FaCEjecutar);
            // 
            // btnC
            // 
            this.btnC.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnC.Location = new System.Drawing.Point(9, 375);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(166, 46);
            this.btnC.TabIndex = 9;
            this.btnC.Text = "F° a  C°";
            this.btnC.UseVisualStyleBackColor = false;
            this.btnC.Click += new System.EventHandler(this.CaFEjecutar);
            // 
            // temaHe1
            // 
            this.temaHe1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temaHe1.BackColor = System.Drawing.Color.GreenYellow;
            this.temaHe1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temaHe1.Location = new System.Drawing.Point(0, 318);
            this.temaHe1.Margin = new System.Windows.Forms.Padding(0);
            this.temaHe1.Name = "temaHe1";
            this.temaHe1.Size = new System.Drawing.Size(258, 39);
            this.temaHe1.TabIndex = 9;
            this.temaHe1.Text = "Herramientas";
            this.temaHe1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // temaF
            // 
            this.temaF.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temaF.BackColor = System.Drawing.Color.GreenYellow;
            this.temaF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temaF.Location = new System.Drawing.Point(6, 201);
            this.temaF.Margin = new System.Windows.Forms.Padding(0);
            this.temaF.Name = "temaF";
            this.temaF.Size = new System.Drawing.Size(249, 38);
            this.temaF.TabIndex = 10;
            this.temaF.Text = "C° a F°";
            this.temaF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // temaC
            // 
            this.temaC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temaC.BackColor = System.Drawing.Color.GreenYellow;
            this.temaC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temaC.Location = new System.Drawing.Point(6, 34);
            this.temaC.Margin = new System.Windows.Forms.Padding(0);
            this.temaC.Name = "temaC";
            this.temaC.Size = new System.Drawing.Size(249, 38);
            this.temaC.TabIndex = 9;
            this.temaC.Text = " F° a  C° ";
            this.temaC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxPrimeros
            // 
            this.groupBoxPrimeros.BackColor = System.Drawing.Color.CornflowerBlue;
            this.groupBoxPrimeros.Controls.Add(this.btnEliminarRaiz);
            this.groupBoxPrimeros.Controls.Add(this.btnEliminarPrimos);
            this.groupBoxPrimeros.Controls.Add(this.listboxRaiz);
            this.groupBoxPrimeros.Controls.Add(this.listboxPrimos);
            this.groupBoxPrimeros.Controls.Add(this.btnListaR);
            this.groupBoxPrimeros.Controls.Add(this.btnListaP);
            this.groupBoxPrimeros.Controls.Add(this.temaRaiz);
            this.groupBoxPrimeros.Controls.Add(this.temaPrimos);
            this.groupBoxPrimeros.Location = new System.Drawing.Point(551, 12);
            this.groupBoxPrimeros.Name = "groupBoxPrimeros";
            this.groupBoxPrimeros.Size = new System.Drawing.Size(253, 523);
            this.groupBoxPrimeros.TabIndex = 2;
            this.groupBoxPrimeros.TabStop = false;
            this.groupBoxPrimeros.Text = "10 Primeros Numeros";
            // 
            // btnListaR
            // 
            this.btnListaR.BackColor = System.Drawing.Color.Peru;
            this.btnListaR.Location = new System.Drawing.Point(9, 482);
            this.btnListaR.Name = "btnListaR";
            this.btnListaR.Size = new System.Drawing.Size(182, 35);
            this.btnListaR.TabIndex = 15;
            this.btnListaR.Text = "Raiz";
            this.btnListaR.UseVisualStyleBackColor = false;
            this.btnListaR.Click += new System.EventHandler(this.btnRaiz);
            // 
            // btnListaP
            // 
            this.btnListaP.BackColor = System.Drawing.Color.Peru;
            this.btnListaP.Location = new System.Drawing.Point(9, 440);
            this.btnListaP.Name = "btnListaP";
            this.btnListaP.Size = new System.Drawing.Size(182, 35);
            this.btnListaP.TabIndex = 13;
            this.btnListaP.Text = "Primos";
            this.btnListaP.UseVisualStyleBackColor = false;
            this.btnListaP.Click += new System.EventHandler(this.btnPrimo);
            // 
            // temaRaiz
            // 
            this.temaRaiz.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temaRaiz.BackColor = System.Drawing.Color.GreenYellow;
            this.temaRaiz.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temaRaiz.Location = new System.Drawing.Point(3, 237);
            this.temaRaiz.Margin = new System.Windows.Forms.Padding(0);
            this.temaRaiz.Name = "temaRaiz";
            this.temaRaiz.Size = new System.Drawing.Size(249, 38);
            this.temaRaiz.TabIndex = 14;
            this.temaRaiz.Text = "Listar Raiz";
            this.temaRaiz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // temaPrimos
            // 
            this.temaPrimos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temaPrimos.BackColor = System.Drawing.Color.GreenYellow;
            this.temaPrimos.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temaPrimos.Location = new System.Drawing.Point(1, 34);
            this.temaPrimos.Margin = new System.Windows.Forms.Padding(0);
            this.temaPrimos.Name = "temaPrimos";
            this.temaPrimos.Size = new System.Drawing.Size(249, 38);
            this.temaPrimos.TabIndex = 13;
            this.temaPrimos.Text = "Listar Primos";
            this.temaPrimos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.temaPrimos.Click += new System.EventHandler(this.label8_Click);
            // 
            // listboxPrimos
            // 
            this.listboxPrimos.FormattingEnabled = true;
            this.listboxPrimos.ItemHeight = 16;
            this.listboxPrimos.Location = new System.Drawing.Point(6, 85);
            this.listboxPrimos.Name = "listboxPrimos";
            this.listboxPrimos.Size = new System.Drawing.Size(238, 132);
            this.listboxPrimos.TabIndex = 16;
            // 
            // listboxRaiz
            // 
            this.listboxRaiz.FormattingEnabled = true;
            this.listboxRaiz.ItemHeight = 16;
            this.listboxRaiz.Location = new System.Drawing.Point(6, 289);
            this.listboxRaiz.Name = "listboxRaiz";
            this.listboxRaiz.Size = new System.Drawing.Size(238, 132);
            this.listboxRaiz.TabIndex = 17;
            // 
            // btnEliminarPrimos
            // 
            this.btnEliminarPrimos.BackColor = System.Drawing.Color.Brown;
            this.btnEliminarPrimos.BackgroundImage = global::Laboratorio1_App01_V1_Interfaz.Properties.Resources.basura1;
            this.btnEliminarPrimos.Location = new System.Drawing.Point(210, 440);
            this.btnEliminarPrimos.Name = "btnEliminarPrimos";
            this.btnEliminarPrimos.Size = new System.Drawing.Size(34, 35);
            this.btnEliminarPrimos.TabIndex = 15;
            this.btnEliminarPrimos.UseVisualStyleBackColor = false;
            this.btnEliminarPrimos.Click += new System.EventHandler(this.btnEliminarPrimos_Click);
            // 
            // btnEliminarRaiz
            // 
            this.btnEliminarRaiz.BackColor = System.Drawing.Color.Brown;
            this.btnEliminarRaiz.BackgroundImage = global::Laboratorio1_App01_V1_Interfaz.Properties.Resources.basura1;
            this.btnEliminarRaiz.Location = new System.Drawing.Point(210, 481);
            this.btnEliminarRaiz.Name = "btnEliminarRaiz";
            this.btnEliminarRaiz.Size = new System.Drawing.Size(34, 35);
            this.btnEliminarRaiz.TabIndex = 18;
            this.btnEliminarRaiz.UseVisualStyleBackColor = false;
            this.btnEliminarRaiz.Click += new System.EventHandler(this.btnEliminarRaiz_Click);
            // 
            // CalculoBasico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(817, 547);
            this.Controls.Add(this.groupBoxPrimeros);
            this.Controls.Add(this.groupBoxTemperaturas);
            this.Controls.Add(this.groupBoxBasicas);
            this.Name = "CalculoBasico";
            this.Text = "Calculadora Basica - Albert Huarcaya";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxBasicas.ResumeLayout(false);
            this.groupBoxBasicas.PerformLayout();
            this.groupBoxTemperaturas.ResumeLayout(false);
            this.groupBoxTemperaturas.PerformLayout();
            this.groupBoxPrimeros.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxBasicas;
        private System.Windows.Forms.GroupBox groupBoxTemperaturas;
        private System.Windows.Forms.GroupBox groupBoxPrimeros;
        private System.Windows.Forms.Label temA;
        private System.Windows.Forms.Label temB;
        private System.Windows.Forms.Label temaHe;
        private System.Windows.Forms.Label temaRe;
        private System.Windows.Forms.Button btnDividir;
        private System.Windows.Forms.Button btnMultiplicar;
        private System.Windows.Forms.Button btnRestar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnSumar;
        private System.Windows.Forms.Button btnF;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Label temaHe1;
        private System.Windows.Forms.Label temaF;
        private System.Windows.Forms.Label temaC;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label temaRaiz;
        private System.Windows.Forms.Label temaPrimos;
        private System.Windows.Forms.Button btnListaP;
        private System.Windows.Forms.Button btnListaR;
        private System.Windows.Forms.TextBox datoB;
        private System.Windows.Forms.TextBox datoaA;
        private System.Windows.Forms.TextBox datoF;
        private System.Windows.Forms.TextBox datoC;
        private System.Windows.Forms.TextBox datorespuesta;
        private System.Windows.Forms.TextBox respuestaF;
        private System.Windows.Forms.TextBox respuestaC;
        private System.Windows.Forms.ListBox listboxRaiz;
        private System.Windows.Forms.ListBox listboxPrimos;
        private System.Windows.Forms.Button btnEliminarRaiz;
        private System.Windows.Forms.Button btnEliminarPrimos;
    }
}

