﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//bd
using System.Configuration; // Se debe agregar Referenciar
using System.Data.SqlClient;
using MaterialSkin;

namespace Laboratorio4_Practica
{
    public partial class Sesion : MaterialSkin.Controls.MaterialForm
    {
        public Sesion()
        {
            InitializeComponent();
        }

        // Clase Publica
        public void logins()
        {
            try
            {
                string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
                using (SqlConnection conexion = new SqlConnection(cnn))
                {
                    conexion.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT PersonID,LastName FROM Person WHERE PersonID='"
                        + txtusuario.Text + "'AND LastName='" + txtclave.Text + "'", conexion))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        if (dr.Read())
                        {
                            // MessageBox.Show("Login exitoso");
                            PrincipalMDI principal = new PrincipalMDI();
                            principal.Show();
                        }
                        else
                        {
                            MessageBox.Show("Datos Incorrectos");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        // Boton de Iniciar Sesion
        private void btnLoguin_Click(object sender, EventArgs e)
        {
            logins();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Fondo para cambiar el Form
        private void Sesion_Load(object sender, EventArgs e)
        {

            SkinManager.Theme = MaterialSkinManager.Themes.DARK;
            SkinManager.ColorScheme = new ColorScheme(Primary.Red700, Primary.Indigo900, Primary.Blue900, Accent.Green700, TextShade.WHITE);
        }

    }
}
