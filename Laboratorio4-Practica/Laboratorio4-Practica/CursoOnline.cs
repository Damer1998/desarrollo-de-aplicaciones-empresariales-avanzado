﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Diseño
using MaterialSkin;

// Conexion
using System.Data.SqlClient;

using System.Configuration;
namespace Laboratorio4_Practica
{
    public partial class CursoOnline : MaterialSkin.Controls.MaterialForm
    {
        public CursoOnline()
        {
            InitializeComponent();
        }

        // Form Curso Online
        private void CursoOnline_Load(object sender, EventArgs e)
        {

        }

        private void btnListarCOL_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {
                conexion.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM OnlineCourse", conexion))
                {


                    MessageBox.Show("Ingreso de Tabla Curso Online Exitoso");
                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoCOL.DataSource = dt;
                    dgvListadoCOL.Refresh();
                }



            }
        }

        private void btnBuscarCOL_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {

                conexion.Open();


                using (SqlCommand cmd = new SqlCommand())
                {
                    String CourseID = txtNombreCOL.Text;
                    cmd.CommandText = "BuscarCourseIDLine";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;


                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@CourseID";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Value = CourseID;

                    cmd.Parameters.Add(param);

                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoCOL.DataSource = dt;
                    dgvListadoCOL.Refresh();

                }

            }
        }
    }
}
