﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Diseño
using MaterialSkin;

// Conexion
using System.Data.SqlClient;

using System.Configuration;


namespace Laboratorio4_Practica
{
    public partial class GradoCurso : MaterialSkin.Controls.MaterialForm
    {
        public GradoCurso()
        {
            InitializeComponent();
        }

        // Form Grado de Curso
        private void GradoCurso_Load(object sender, EventArgs e)
        {

        }

        private void btnListarGU_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {
                conexion.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM CourseGrade", conexion))
                {


                    MessageBox.Show("Ingreso de Tabla Grado de Curso Exitoso");
                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoGU.DataSource = dt;
                    dgvListadoGU.Refresh();
                }



            }
        }

        private void btnBuscarGU_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {

                conexion.Open();


                using (SqlCommand cmd = new SqlCommand())
                {
                    String Title = txtNombreGU.Text;
                    cmd.CommandText = "BuscarCourseGrade";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;


                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@EnrollmentID";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Value = Title;

                    cmd.Parameters.Add(param);

                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoGU.DataSource = dt;
                    dgvListadoGU.Refresh();

                }

            }
        }
    }
}
