﻿namespace Laboratorio4_Practica
{
    partial class InstructorCurso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvListadoIU = new System.Windows.Forms.DataGridView();
            this.btnBuscarIU = new System.Windows.Forms.Button();
            this.btnListarIU = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombreIU = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoIU)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvListadoIU
            // 
            this.dgvListadoIU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListadoIU.Location = new System.Drawing.Point(45, 294);
            this.dgvListadoIU.Name = "dgvListadoIU";
            this.dgvListadoIU.RowHeadersWidth = 51;
            this.dgvListadoIU.RowTemplate.Height = 24;
            this.dgvListadoIU.Size = new System.Drawing.Size(776, 394);
            this.dgvListadoIU.TabIndex = 83;
            // 
            // btnBuscarIU
            // 
            this.btnBuscarIU.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBuscarIU.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarIU.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarIU.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBuscarIU.Location = new System.Drawing.Point(461, 228);
            this.btnBuscarIU.Name = "btnBuscarIU";
            this.btnBuscarIU.Size = new System.Drawing.Size(360, 34);
            this.btnBuscarIU.TabIndex = 82;
            this.btnBuscarIU.Text = "Buscar";
            this.btnBuscarIU.UseVisualStyleBackColor = false;
            this.btnBuscarIU.Click += new System.EventHandler(this.btnBuscarIU_Click);
            // 
            // btnListarIU
            // 
            this.btnListarIU.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnListarIU.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarIU.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarIU.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnListarIU.Location = new System.Drawing.Point(45, 228);
            this.btnListarIU.Name = "btnListarIU";
            this.btnListarIU.Size = new System.Drawing.Size(387, 34);
            this.btnListarIU.TabIndex = 81;
            this.btnListarIU.Text = "Listar";
            this.btnListarIU.UseVisualStyleBackColor = false;
            this.btnListarIU.Click += new System.EventHandler(this.btnListarIU_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtNombreIU);
            this.panel2.Location = new System.Drawing.Point(45, 128);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 94);
            this.panel2.TabIndex = 80;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(14, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 19);
            this.label3.TabIndex = 10;
            this.label3.Text = "Busqueda";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(776, 30);
            this.label6.TabIndex = 16;
            this.label6.Text = "Herramienta Rapida - Instructor";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNombreIU
            // 
            this.txtNombreIU.Location = new System.Drawing.Point(111, 43);
            this.txtNombreIU.Name = "txtNombreIU";
            this.txtNombreIU.Size = new System.Drawing.Size(650, 22);
            this.txtNombreIU.TabIndex = 11;
            // 
            // InstructorCurso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 748);
            this.Controls.Add(this.dgvListadoIU);
            this.Controls.Add(this.btnBuscarIU);
            this.Controls.Add(this.btnListarIU);
            this.Controls.Add(this.panel2);
            this.Name = "InstructorCurso";
            this.Text = "InstructorCurso";
            this.Load += new System.EventHandler(this.InstructorCurso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoIU)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvListadoIU;
        private System.Windows.Forms.Button btnBuscarIU;
        private System.Windows.Forms.Button btnListarIU;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombreIU;
    }
}