﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño
using MaterialSkin;

// Conexion
using System.Data.SqlClient;

using System.Configuration;


namespace Laboratorio4_Practica
{
    public partial class Departamento : MaterialSkin.Controls.MaterialForm
    {
        public Departamento()
        {
            InitializeComponent();
        }

        private void Departamento_Load(object sender, EventArgs e)
        {

        }

        private void btnListarD_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {
                conexion.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Department", conexion))
                {


                    MessageBox.Show("Ingreso de Tabla Department Exitoso");
                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoD.DataSource = dt;
                    dgvListadoD.Refresh();
                }



            }
        }

        private void btnBuscarD_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {

                conexion.Open();


                using (SqlCommand cmd = new SqlCommand())
                {
                    String Nombre = txtNombreD.Text;
                    cmd.CommandText = "BuscarName";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;


                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Name";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Value = Nombre;

                    cmd.Parameters.Add(param);

                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoD.DataSource = dt;
                    dgvListadoD.Refresh();

                }

            }
        }
    }
}
