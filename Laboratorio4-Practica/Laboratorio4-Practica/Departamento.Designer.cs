﻿namespace Laboratorio4_Practica
{
    partial class Departamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvListadoD = new System.Windows.Forms.DataGridView();
            this.btnBuscarD = new System.Windows.Forms.Button();
            this.btnListarD = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombreD = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoD)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvListadoD
            // 
            this.dgvListadoD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListadoD.Location = new System.Drawing.Point(45, 291);
            this.dgvListadoD.Name = "dgvListadoD";
            this.dgvListadoD.RowHeadersWidth = 51;
            this.dgvListadoD.RowTemplate.Height = 24;
            this.dgvListadoD.Size = new System.Drawing.Size(776, 394);
            this.dgvListadoD.TabIndex = 91;
            // 
            // btnBuscarD
            // 
            this.btnBuscarD.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBuscarD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarD.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarD.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBuscarD.Location = new System.Drawing.Point(461, 225);
            this.btnBuscarD.Name = "btnBuscarD";
            this.btnBuscarD.Size = new System.Drawing.Size(360, 34);
            this.btnBuscarD.TabIndex = 90;
            this.btnBuscarD.Text = "Buscar";
            this.btnBuscarD.UseVisualStyleBackColor = false;
            this.btnBuscarD.Click += new System.EventHandler(this.btnBuscarD_Click);
            // 
            // btnListarD
            // 
            this.btnListarD.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnListarD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarD.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarD.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnListarD.Location = new System.Drawing.Point(45, 225);
            this.btnListarD.Name = "btnListarD";
            this.btnListarD.Size = new System.Drawing.Size(387, 34);
            this.btnListarD.TabIndex = 89;
            this.btnListarD.Text = "Listar";
            this.btnListarD.UseVisualStyleBackColor = false;
            this.btnListarD.Click += new System.EventHandler(this.btnListarD_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtNombreD);
            this.panel2.Location = new System.Drawing.Point(45, 125);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 94);
            this.panel2.TabIndex = 88;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(14, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 19);
            this.label3.TabIndex = 10;
            this.label3.Text = "Busqueda";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(776, 30);
            this.label6.TabIndex = 16;
            this.label6.Text = "Herramienta Rapida - Departamento";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNombreD
            // 
            this.txtNombreD.Location = new System.Drawing.Point(111, 43);
            this.txtNombreD.Name = "txtNombreD";
            this.txtNombreD.Size = new System.Drawing.Size(650, 22);
            this.txtNombreD.TabIndex = 11;
            // 
            // Departamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 748);
            this.Controls.Add(this.dgvListadoD);
            this.Controls.Add(this.btnBuscarD);
            this.Controls.Add(this.btnListarD);
            this.Controls.Add(this.panel2);
            this.Name = "Departamento";
            this.Text = "Departamento";
            this.Load += new System.EventHandler(this.Departamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoD)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvListadoD;
        private System.Windows.Forms.Button btnBuscarD;
        private System.Windows.Forms.Button btnListarD;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombreD;
    }
}