﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño
using MaterialSkin;

// Conexion
using System.Data.SqlClient;

using System.Configuration;

namespace Laboratorio4_Practica
{
    public partial class CursoOnsite : MaterialSkin.Controls.MaterialForm
    {



     


        public CursoOnsite()
        {
            InitializeComponent();
 
        }
        // Form Curso Onsite
        private void CursoOnsite_Load(object sender, EventArgs e)
        {

        }

        private void btnListarCO_Click(object sender, EventArgs e)
        {

            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {
                conexion.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM OnsiteCourse", conexion))
                {


                    MessageBox.Show("Login exitoso");
                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoCO.DataSource = dt;
                    dgvListadoCO.Refresh();
                }



            }

        }

        private void btnBuscarCO_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {

                conexion.Open();


                using (SqlCommand cmd = new SqlCommand())
                {
                    String CourseID = txtNombreCO.Text;
                    cmd.CommandText = "BuscarCourseID";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;


                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@CourseID";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Value = CourseID;

                    cmd.Parameters.Add(param);

                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoCO.DataSource = dt;
                    dgvListadoCO.Refresh();

                }

            }
        }
    }
}
