﻿namespace Laboratorio4_Practica
{
    partial class GradoCurso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvListadoGU = new System.Windows.Forms.DataGridView();
            this.btnBuscarGU = new System.Windows.Forms.Button();
            this.btnListarGU = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombreGU = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoGU)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvListadoGU
            // 
            this.dgvListadoGU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListadoGU.Location = new System.Drawing.Point(39, 287);
            this.dgvListadoGU.Name = "dgvListadoGU";
            this.dgvListadoGU.RowHeadersWidth = 51;
            this.dgvListadoGU.RowTemplate.Height = 24;
            this.dgvListadoGU.Size = new System.Drawing.Size(776, 394);
            this.dgvListadoGU.TabIndex = 87;
            // 
            // btnBuscarGU
            // 
            this.btnBuscarGU.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBuscarGU.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarGU.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarGU.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBuscarGU.Location = new System.Drawing.Point(455, 221);
            this.btnBuscarGU.Name = "btnBuscarGU";
            this.btnBuscarGU.Size = new System.Drawing.Size(360, 34);
            this.btnBuscarGU.TabIndex = 86;
            this.btnBuscarGU.Text = "Buscar";
            this.btnBuscarGU.UseVisualStyleBackColor = false;
            this.btnBuscarGU.Click += new System.EventHandler(this.btnBuscarGU_Click);
            // 
            // btnListarGU
            // 
            this.btnListarGU.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnListarGU.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarGU.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarGU.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnListarGU.Location = new System.Drawing.Point(39, 221);
            this.btnListarGU.Name = "btnListarGU";
            this.btnListarGU.Size = new System.Drawing.Size(387, 34);
            this.btnListarGU.TabIndex = 85;
            this.btnListarGU.Text = "Listar";
            this.btnListarGU.UseVisualStyleBackColor = false;
            this.btnListarGU.Click += new System.EventHandler(this.btnListarGU_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtNombreGU);
            this.panel2.Location = new System.Drawing.Point(39, 121);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 94);
            this.panel2.TabIndex = 84;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(14, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 19);
            this.label3.TabIndex = 10;
            this.label3.Text = "Busqueda";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(776, 30);
            this.label6.TabIndex = 16;
            this.label6.Text = "Herramienta Rapida - Grado";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNombreGU
            // 
            this.txtNombreGU.Location = new System.Drawing.Point(111, 43);
            this.txtNombreGU.Name = "txtNombreGU";
            this.txtNombreGU.Size = new System.Drawing.Size(650, 22);
            this.txtNombreGU.TabIndex = 11;
            // 
            // GradoCurso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 748);
            this.Controls.Add(this.dgvListadoGU);
            this.Controls.Add(this.btnBuscarGU);
            this.Controls.Add(this.btnListarGU);
            this.Controls.Add(this.panel2);
            this.Name = "GradoCurso";
            this.Text = "GradoCurso";
            this.Load += new System.EventHandler(this.GradoCurso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoGU)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvListadoGU;
        private System.Windows.Forms.Button btnBuscarGU;
        private System.Windows.Forms.Button btnListarGU;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombreGU;
    }
}