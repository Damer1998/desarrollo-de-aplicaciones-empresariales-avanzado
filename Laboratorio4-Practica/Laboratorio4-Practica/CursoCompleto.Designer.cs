﻿namespace Laboratorio4_Practica
{
    partial class CursoCompleto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvListadoCC = new System.Windows.Forms.DataGridView();
            this.btnBuscarCC = new System.Windows.Forms.Button();
            this.btnListarCC = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombreCC = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoCC)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvListadoCC
            // 
            this.dgvListadoCC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListadoCC.Location = new System.Drawing.Point(51, 297);
            this.dgvListadoCC.Name = "dgvListadoCC";
            this.dgvListadoCC.RowHeadersWidth = 51;
            this.dgvListadoCC.RowTemplate.Height = 24;
            this.dgvListadoCC.Size = new System.Drawing.Size(776, 394);
            this.dgvListadoCC.TabIndex = 71;
            // 
            // btnBuscarCC
            // 
            this.btnBuscarCC.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBuscarCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarCC.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarCC.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBuscarCC.Location = new System.Drawing.Point(467, 231);
            this.btnBuscarCC.Name = "btnBuscarCC";
            this.btnBuscarCC.Size = new System.Drawing.Size(360, 34);
            this.btnBuscarCC.TabIndex = 70;
            this.btnBuscarCC.Text = "Buscar";
            this.btnBuscarCC.UseVisualStyleBackColor = false;
            this.btnBuscarCC.Click += new System.EventHandler(this.btnBuscarCC_Click);
            // 
            // btnListarCC
            // 
            this.btnListarCC.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnListarCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarCC.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarCC.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnListarCC.Location = new System.Drawing.Point(51, 231);
            this.btnListarCC.Name = "btnListarCC";
            this.btnListarCC.Size = new System.Drawing.Size(387, 34);
            this.btnListarCC.TabIndex = 69;
            this.btnListarCC.Text = "Listar";
            this.btnListarCC.UseVisualStyleBackColor = false;
            this.btnListarCC.Click += new System.EventHandler(this.btnListarCC_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtNombreCC);
            this.panel2.Location = new System.Drawing.Point(51, 131);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 94);
            this.panel2.TabIndex = 68;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(14, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 19);
            this.label3.TabIndex = 10;
            this.label3.Text = "Busqueda";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(776, 30);
            this.label6.TabIndex = 16;
            this.label6.Text = "Herramienta Rapida - Curso Completo";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNombreCC
            // 
            this.txtNombreCC.Location = new System.Drawing.Point(111, 43);
            this.txtNombreCC.Name = "txtNombreCC";
            this.txtNombreCC.Size = new System.Drawing.Size(650, 22);
            this.txtNombreCC.TabIndex = 11;
            // 
            // CursoCompleto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 748);
            this.Controls.Add(this.dgvListadoCC);
            this.Controls.Add(this.btnBuscarCC);
            this.Controls.Add(this.btnListarCC);
            this.Controls.Add(this.panel2);
            this.Name = "CursoCompleto";
            this.Text = "CursoCompleto";
            this.Load += new System.EventHandler(this.CursoCompleto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoCC)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvListadoCC;
        private System.Windows.Forms.Button btnBuscarCC;
        private System.Windows.Forms.Button btnListarCC;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombreCC;
    }
}