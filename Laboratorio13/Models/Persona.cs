﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Laboratorio13.Models
{
    // Modelado para la tabla Persona
    public class Persona
    {


        public int PersonaID { get; set;}
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaNac { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
      
    }



}

/*
 * 
 * 
 * 
 * 
<!--
<table class="table table-striped">
    <tr>
        <th>PersonaID </th>
        <th>Nombre </th>
        <th>Apellido </th>
        <th>Fecha Nac </th>
        <th>Direccion </th>
        <th>Email </th>
        <th>Acciones </th>
    </tr>
    @foreach (var persona in Model)
    {
<tr>
    <td>@persona.PersonaID</td>
    <td>@persona.Nombre}</td>
    <td>@persona.Apellido</td>
    <td>@persona.FechaNac</td>
    <td>@persona.Direccion</td>
    <td>@persona.Email</td>
    <td>@Html.ActionLink("Ver","Mostrar","Persona", new { id = persona.PersonaID},null)</td>
</tr>
    }



</table>
    -->*/