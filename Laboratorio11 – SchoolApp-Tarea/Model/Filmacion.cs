﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Filmacion
    {

        public int ID { get; set; }
        public string Title { get; set; }
        public string Director { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}
