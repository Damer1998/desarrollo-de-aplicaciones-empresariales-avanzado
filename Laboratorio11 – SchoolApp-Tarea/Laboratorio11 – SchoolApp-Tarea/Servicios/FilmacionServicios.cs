﻿using Data.Repositorio;
using Laboratorio11___SchoolApp_Tarea.Data;
using Laboratorio11___SchoolApp_Tarea.Interfaces;
using Model;
using System;
using System.Collections.Generic;

using System.Threading.Tasks;

namespace Laboratorio11___SchoolApp_Tarea.Servicios
{
    public class FilmacionServicios : IFilmacionServicio
    {


        private readonly SqlConfiguration _configuration;
        private IFilmRepositorio _FilmRepositorio;
        public FilmacionServicios(SqlConfiguration configuration)
        {
            _configuration = configuration;
            _FilmRepositorio = new FilmRepositorio(configuration.ConnectionString);
        }





        public Task<bool> EliminarFilmacion(int id)
        {
            return _FilmRepositorio.EliminarFilmacion(id);
        }

        public Task<IEnumerable<Filmacion>> GetAllFilms()
        {
            return _FilmRepositorio.GetAllFilms();
        }

        public Task<Filmacion> GetFilmacionDetalles(int id)
        {

            return _FilmRepositorio.GetFilmacionDetalles(id);
        }

        public Task<bool> GuardarFilmacion(Filmacion film)
        {
            if (film.ID == 0)
                return _FilmRepositorio.InsertarFilmacion(film);
            else
                return _FilmRepositorio.ActualizarFilmacion(film);
        }
    }
}
