﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Laboratorio11___SchoolApp_Tarea.Interfaces
{
    public interface IFilmacionServicio 
    {

        Task<IEnumerable<Filmacion>> GetAllFilms();
        Task<Filmacion> GetFilmacionDetalles(int id);
        Task<bool> EliminarFilmacion(int id);
        Task<bool> GuardarFilmacion(Filmacion film);
    }
}
