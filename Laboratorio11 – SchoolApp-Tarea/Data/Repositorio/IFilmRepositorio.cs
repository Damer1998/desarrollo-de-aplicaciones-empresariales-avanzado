﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Model;

// Interfaz
namespace Data.Repositorio
{
    public interface IFilmRepositorio
    {

        Task<IEnumerable<Filmacion>> GetAllFilms();
        Task<Filmacion> GetFilmacionDetalles(int id);
        Task<bool> InsertarFilmacion(Filmacion film);
        Task<bool> ActualizarFilmacion(Filmacion film);
        Task<bool> EliminarFilmacion(int id);
    }
}
