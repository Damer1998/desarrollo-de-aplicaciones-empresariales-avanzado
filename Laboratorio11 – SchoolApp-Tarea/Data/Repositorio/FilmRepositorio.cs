﻿using Dapper;
using Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositorio
{
    public class FilmRepositorio : IFilmRepositorio
    {

        private string ConnectionString;

        public FilmRepositorio(string connectionString)
        {
            ConnectionString = connectionString;
        }

        protected SqlConnection dbConnection()
        {
            return new SqlConnection(ConnectionString);
        }


        public async Task<bool> ActualizarFilmacion(Filmacion film)
        {
            var db = dbConnection();
            var sql = @"UPDATE Films SET Title = @Title , Director = @Director , ReleaseDate =  @ReleaseDate WHERE id=@ID";

            var result = await db.ExecuteAsync(sql.ToString(), new
            {
                film.Title,
                film.Director,
                film.ReleaseDate,
                film.ID
            });
            return result > 0;
        }

        public async Task<bool> EliminarFilmacion(int id)
        {
            var db = dbConnection();
            var sql = @"DELETE FROM Films WHERE id = @ID";

            var result =  await db.ExecuteAsync(sql.ToString(), new { ID = id });

            return result > 0;
        }

        public async Task<IEnumerable<Filmacion>> GetAllFilms()
        {
            var db = dbConnection();
            var sql = @"SELECT id , Title , Director , ReleaseDate   FROM [dbo].[Films]";

            return await db.QueryAsync<Filmacion>(sql.ToString(), new { });
        }

        public async Task<Filmacion> GetFilmacionDetalles(int id)
        {
            var db = dbConnection();
            var sql = @"SELECT id , Title , Director , ReleaseDate   FROM [dbo].[Films] WHERE id = @ID";

            return await db.QueryFirstOrDefaultAsync<Filmacion>(sql.ToString(), new {  ID= id });
        }

        public async Task<bool> InsertarFilmacion(Filmacion film)
        {
            var db = dbConnection();
            var sql = @"INSERT INTO Films (Title , Director , ReleaseDate)
            VALUES(@Title , @Director, @ReleaseDate)";

            var result = await db.ExecuteAsync(sql.ToString(), new
            {
                film.Title,
                film.Director,
                film.ReleaseDate
            });
            return result > 0;
        }
    }
}
