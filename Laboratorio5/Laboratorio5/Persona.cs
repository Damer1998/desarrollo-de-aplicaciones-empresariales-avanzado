﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// SQL


using System.Data.SqlClient;


namespace Laboratorio5
{
    public partial class ManPerson : Form
    {

        SqlConnection con;

        public ManPerson()
        {
            InitializeComponent();
        }

        private void ManPerson_Load(object sender, EventArgs e)
        {
            String str = "Server=LAPTOP-1T0G2E8E;DataBase=Escuela;Integrated Security=true;";
            con = new SqlConnection(str);
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            con.Open();
            String sql = "SELECT * FROM Person";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            DataTable dt = new DataTable();
            dt.Load(reader);

            dgvListado.DataSource = dt;
            dgvListado.Refresh();
            con.Close();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            con.Open();
            String sp = "InsertPerson";
            //String sp = "InsertPersona";
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.StoredProcedure;

          //  cmd.Parameters.AddWithValue("@PersonID", Convert.ToInt32(txtPersonID.Text));
            cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
            // cmd.Parameters.AddWithValue("@HireDate", Convert.ToDateTime(txtHireDate.Text));
            cmd.Parameters.AddWithValue("@HireDate", txtHireDate.Value.Date);
            //  cmd.Parameters.AddWithValue("@EnrollmentDate", Convert.ToDateTime(txtEnrollmentDate.Text));
            cmd.Parameters.AddWithValue("@EnrollmentDate", txtEnrollmentDate.Value.Date);
            

            // Inicia como contador el PersonId
            int codigo = Convert.ToInt32(cmd.ExecuteScalar());
            MessageBox.Show("Se ha registrado nueva persona con el codigo " + codigo );
            con.Close();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            con.Open();
            String sp = "UpdatePerson";
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@PersonID", txtPersonID.Text);
           // cmd.Parameters.AddWithValue("@PersonID", Convert.ToInt32(txtPersonID.Text));
            cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
            // cmd.Parameters.AddWithValue("@HireDate", Convert.ToDateTime(txtHireDate.Text));
            // cmd.Parameters.AddWithValue("@EnrollmentDate", Convert.ToDateTime(txtEnrollmentDate.Text));
            cmd.Parameters.AddWithValue("@HireDate", txtHireDate.Value.Date);
            cmd.Parameters.AddWithValue("@EnrollmentDate", txtEnrollmentDate.Value.Date);

            int resultado = cmd.ExecuteNonQuery();
            if (resultado > 0)
            {
                MessageBox.Show("Se ha modificado el registro correctamente");
            }
            con.Close();

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            /*
            con.Open();
            int resultado = 0;
            String sp = "Delete from Person where PersonID = '" + txtPersonID.Text + "'";
            SqlCommand cmd = new SqlCommand(sp, con);

            resultado = cmd.ExecuteNonQuery();
            if (resultado == 1)
            {
                MessageBox.Show("Se ha eliminado el registro correctamente");
            }
            else
            {
                MessageBox.Show("No pudo eliminarse el registro");
            }
            con.Close();
            */

         
            con.Open();
            String sp = "DeletePerson";
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@PersonID", txtPersonID.Text);

            int resultado = cmd.ExecuteNonQuery();
            if (resultado > 0)
            {
                MessageBox.Show("Se ha eliminado el registro correctamente");
            }
            con.Close();
            /*   */
        }

        private void dgvListado_SelectionChanged(object sender, EventArgs e)
        {
            if ( dgvListado.SelectedRows.Count > 0)
            {
                txtPersonID.Text = dgvListado.SelectedRows[0].Cells[0].Value.ToString();
                txtFirstName.Text = dgvListado.SelectedRows[0].Cells[2].Value.ToString();
                txtLastName.Text = dgvListado.SelectedRows[0].Cells[1].Value.ToString();
                txtHireDate.Text = dgvListado.SelectedRows[0].Cells[3].Value.ToString();
                txtEnrollmentDate.Text = dgvListado.SelectedRows[0].Cells[4].Value.ToString();
            }

        }


        // Value Changed para HireDate
        private void txtHireDate_ValueChanged(object sender, EventArgs e)
        {
            txtHireDate.CustomFormat = "dd-MM-yyyy";
        }
        // Value Changed para EnrollmentDat
        private void txtEnrollmentDate_ValueChanged(object sender, EventArgs e)
        {
            txtEnrollmentDate.CustomFormat = "dd-MM-yyyy";
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            con.Open();
            String sql = "SELECT * FROM Person where " + comboBoxBusqueda.Text + " like '%" + textBusqueda.Text + "%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            DataTable dt = new DataTable();
            dt.Load(reader);

            dgvListado.DataSource = dt;
            dgvListado.Refresh();
            con.Close();
        }

 
    }
}
