﻿using Dapper;
using Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositorio
{
    public class FilmRepositorio : IFilmRepositorio
    {

        private string ConnectionString;

        public FilmRepositorio(string connectionString)
        {
            ConnectionString = connectionString;
        }

        protected SqlConnection dbConnection()
        {
            return new SqlConnection(ConnectionString);
        }


        //public  Task<bool> ActualizarFilmacion(Filmacion film)
         public async Task<bool> ActualizarFilmacion(Filmacion film)
        {
            // throw new NotImplementedException();
            /* */
            var db = dbConnection();
            var sql = @"UPDATE clientes SET  
            Paciente=@Paciente,Email=@Email,
            Celular=@Celular,Examen =@Examen,
            Fecha_solicitud=@Fecha_solicitud,Imagen=@Imagen,
            Informe=@Informe,Aseguradora=@Aseguradora
            WHERE Correlativo=@Correlativo";

            var result = await db.ExecuteAsync(sql.ToString(), new
            {
                film.Paciente,
                film.Email,
                film.Celular,
                film.Examen,
                film.Fecha_solicitud,
                film.Imagen,
                film.Informe,
                film.Aseguradora,
                film.Correlativo
            });
            return result > 0;
            
        }

       // public  Task<bool> EliminarFilmacion(int Correlativo)
        public async Task<bool> EliminarFilmacion(int Correlativo)
        {

            //throw new NotImplementedException();

        
            var db = dbConnection();
            var sql = @"DELETE FROM clientes WHERE Correlativo = @correlativo";

            var result = await db.ExecuteAsync(sql.ToString(), new { correlativo = Correlativo });

            return result > 0;
            /* */
        }
        // Listado de Paciente
        public async Task<IEnumerable<Filmacion>> GetAllFilms()
        {
            var db = dbConnection();
           
            var sql = @"SELECT Correlativo , CodigoOA , Paciente , Email ,Celular ,Medico , Aseguradora,Procedencia,Examen,Estado,Fecha_solicitud,
            Edad,Imagen,Informe FROM [dbo].[clientes]"; /**/
            //  var sql = @"SELECT * FROM [dbo].[clientes]";
            return await db.QueryAsync<Filmacion>(sql.ToString(), new { });
        }


        // Listado
        public async Task<Filmacion> GetFilmacionDetalles(int Correlativo)
        {
            var db = dbConnection();
            var sql = @"SELECT Correlativo , CodigoOA , Paciente , Email ,Celular ,Medico , Aseguradora,Procedencia,Examen,Estado,Fecha_solicitud,
            Edad,Imagen,Informe  FROM [dbo].[clientes] WHERE Correlativo = @correlativo";

            return await db.QueryFirstOrDefaultAsync<Filmacion>(sql.ToString(), new { correlativo = Correlativo });
        }

        //    public  Task<bool> InsertarFilmacion(Filmacion film)
        public async Task<bool> InsertarFilmacion(Filmacion film)
        {

          //  throw new NotImplementedException();

          
            var db = dbConnection();
            var sql = @"INSERT INTO clientes (Paciente ,Email ,Celular ,Examen,Fecha_solicitud ,Imagen , Informe , Aseguradora)
            VALUES(@Paciente, @Email ,@Celular , @Examen, @Fecha_solicitud , @Imagen, @Informe, @Aseguradora )";

            var result = await db.ExecuteAsync(sql.ToString(), new
            {

                film.Paciente,
                film.Aseguradora,
                film.Email,
                film.Celular,
                film.Examen,
                film.Fecha_solicitud,
                film.Imagen,
                film.Informe,
                film.Correlativo
            });
            return result > 0;
            /*   */
        }
    }
}
