﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Data.Repositorio
{
    public interface IFilmRepositorio
    {

        Task<IEnumerable<Filmacion>> GetAllFilms();
        Task<Filmacion> GetFilmacionDetalles(int Correlativo);
        Task<bool> InsertarFilmacion(Filmacion film);
        Task<bool> ActualizarFilmacion(Filmacion film);
        Task<bool> EliminarFilmacion(int Correlativo);
    }
}