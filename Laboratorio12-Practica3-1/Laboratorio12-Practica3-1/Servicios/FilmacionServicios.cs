﻿using Data.Repositorio;
using Laboratorio12_Practica3_1.Data;
using Laboratorio12_Practica3_1.Interfaces;
using Model;
using System;
using System.Collections.Generic;

using System.Threading.Tasks;

namespace Laboratorio12_Practica3_1.Servicios
{
    public class FilmacionServicios : IFilmacionServicio
    {


        private readonly SqlConfiguration _configuration;
        private IFilmRepositorio _FilmRepositorio;
        public FilmacionServicios(SqlConfiguration configuration)
        {
            _configuration = configuration;
            _FilmRepositorio = new FilmRepositorio(configuration.ConnectionString);
        }





        public Task<bool> EliminarFilmacion(int Correlativo)
        {
            return _FilmRepositorio.EliminarFilmacion(Correlativo);
        }

        public Task<IEnumerable<Filmacion>> GetAllFilms()
        {
            return _FilmRepositorio.GetAllFilms();
        }

        public Task<Filmacion> GetFilmacionDetalles(int Correlativo)
        {

            return _FilmRepositorio.GetFilmacionDetalles(Correlativo);
        }

        public Task<bool> GuardarFilmacion(Filmacion film)
        {
            if (film.Correlativo == 0)
                return _FilmRepositorio.InsertarFilmacion(film);
            else
                return _FilmRepositorio.ActualizarFilmacion(film);
        }
    }
}
