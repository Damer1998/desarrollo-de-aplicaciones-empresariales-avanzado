﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Model;
namespace Laboratorio12_Practica3_1.Interfaces
{
    public interface IFilmacionServicio
    {

        Task<IEnumerable<Filmacion>> GetAllFilms();
        Task<Filmacion> GetFilmacionDetalles(int Correlativo);
        Task<bool> EliminarFilmacion(int Correlativo);
        Task<bool> GuardarFilmacion(Filmacion film);
    }
}
