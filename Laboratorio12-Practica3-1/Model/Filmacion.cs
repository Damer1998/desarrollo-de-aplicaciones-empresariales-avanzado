﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Filmacion
    {

        public int Correlativo { get; set; } //1
        public string Paciente { get; set; } //2
        public string Aseguradora { get; set; } //3
        public string Examen { get; set; } //4
        public DateTime Fecha_solicitud { get; set; } //5
        public string Imagen { get; set; }//6
        public string Informe { get; set; }//7
        public string Email { get; set; }//8
        public int Celular { get; set; }//9


    }
}
