﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Laboratorio12.Models;

namespace Laboratorio12.Controllers
{
    public class PersonaController : Controller
    {
        // GET: Persona
        public ActionResult Index()
        {

            return View();

    }

        public ActionResult Listar()
        { 
        List<Persona> personas = new List<Persona>();
        personas.Add(new Persona
            {
                PersonaID =1,
                Nombre = "Alberto",
                Apellido = "Huarcaya",
                Direccion = "Av.Peru Evergrenn 125",
                FechaNac = Convert.ToDateTime("1997-11-08"),
                Email = "albert.huarcaya@tecsup.edu.pe"

            });
            personas.Add(new Persona
            {
                PersonaID = 2,
                Nombre = "Maria",
                Apellido = "huarcaya",
                Direccion = "Av.Peru San Lorenzo 125",
                FechaNac = Convert.ToDateTime("1998-11-05"),
                Email = "maria.huarcaya@tecsup.edu.pe"

            });

            personas.Add(new Persona
            {
                PersonaID = 3,
                Nombre = "Fernanda",
                Apellido = "Salomar",
                Direccion = "Av.Peru Chilina 125",
                FechaNac = Convert.ToDateTime("1998-05-05"),
                Email = "ferna.salo@tecsup.edu.pe"

            });


            return View(personas);
        }

    }
}