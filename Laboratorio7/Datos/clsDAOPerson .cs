﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Datos
{

    public class clsDAOPerson : clsDAO
    {
        public DataTable GetAll()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarA()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'A%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }



        public DataTable GetAllBuscarB()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'B%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarC()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'C%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarD()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'D%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarE()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'E%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarF()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'F%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarG()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'G%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarH()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'H%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarI()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'I%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarJ()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'J%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarK()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'K%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarL()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'L%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarM()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'M%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarN()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'N%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarO()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'O%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarP()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'P%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarQ()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'Q%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }



        public DataTable GetAllBuscarR()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'R%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarS()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'S%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarT()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'T%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }



        public DataTable GetAllBuscarU()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'U%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAllBuscarV()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'V%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarW()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'W%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }


        public DataTable GetAllBuscarX()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'X%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }



        public DataTable GetAllBuscarY()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'Y%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }



        public DataTable GetAllBuscarZ()
        {
            DataTable dt = new DataTable();

            con.Open();
            String sql = "SELECT * FROM Person where  FirstName like 'Z%'";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();

            dt.Load(reader);
            con.Close();
            return dt;
        }
















    }
}
