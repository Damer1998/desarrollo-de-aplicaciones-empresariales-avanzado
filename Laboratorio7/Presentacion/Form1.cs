﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using Negocio;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dt = np.GetAll();

            dgDatos.DataSource = dt;
            dgDatos.Refresh();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {

            DataTable dtBuscarA = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarA = np.GetAllBuscarNombreA();

            dgDatos.DataSource = dtBuscarA;
            dgDatos.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarB = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarB = np.GetAllBuscarNombreB();

            dgDatos.DataSource = dtBuscarB;
            dgDatos.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarC = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarC = np.GetAllBuscarNombreC();

            dgDatos.DataSource = dtBuscarC;
            dgDatos.Refresh();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarD = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarD = np.GetAllBuscarNombreD();

            dgDatos.DataSource = dtBuscarD;
            dgDatos.Refresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarE = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarE = np.GetAllBuscarNombreE();

            dgDatos.DataSource = dtBuscarE;
            dgDatos.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarF = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarF = np.GetAllBuscarNombreF();

            dgDatos.DataSource = dtBuscarF;
            dgDatos.Refresh();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarG = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarG = np.GetAllBuscarNombreG();

            dgDatos.DataSource = dtBuscarG;
            dgDatos.Refresh();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarH = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarH = np.GetAllBuscarNombreH();

            dgDatos.DataSource = dtBuscarH;
            dgDatos.Refresh();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarI = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarI = np.GetAllBuscarNombreI();

            dgDatos.DataSource = dtBuscarI;
            dgDatos.Refresh();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarJ = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarJ = np.GetAllBuscarNombreJ();

            dgDatos.DataSource = dtBuscarJ;
            dgDatos.Refresh();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarK = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarK = np.GetAllBuscarNombreK();

            dgDatos.DataSource = dtBuscarK;
            dgDatos.Refresh();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarL = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarL = np.GetAllBuscarNombreL();

            dgDatos.DataSource = dtBuscarL;
            dgDatos.Refresh();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarM = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarM = np.GetAllBuscarNombreM();

            dgDatos.DataSource = dtBuscarM;
            dgDatos.Refresh();
        }
        //
        private void button25_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarN = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarN = np.GetAllBuscarNombreN();

            dgDatos.DataSource = dtBuscarN;
            dgDatos.Refresh();
        }

        private void button23_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarO = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarO = np.GetAllBuscarNombreO();

            dgDatos.DataSource = dtBuscarO;
            dgDatos.Refresh();
        }

        private void button22_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarP = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarP = np.GetAllBuscarNombreP();

            dgDatos.DataSource = dtBuscarP;
            dgDatos.Refresh();
        }

        private void button21_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarQ = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarQ = np.GetAllBuscarNombreQ();

            dgDatos.DataSource = dtBuscarQ;
            dgDatos.Refresh();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarR = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarR = np.GetAllBuscarNombreR();

            dgDatos.DataSource = dtBuscarR;
            dgDatos.Refresh();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarS = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarS = np.GetAllBuscarNombreS();

            dgDatos.DataSource = dtBuscarS;
            dgDatos.Refresh();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarT = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarT = np.GetAllBuscarNombreT();

            dgDatos.DataSource = dtBuscarT;
            dgDatos.Refresh();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarU = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarU = np.GetAllBuscarNombreM();

            dgDatos.DataSource = dtBuscarU;
            dgDatos.Refresh();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarV = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarV = np.GetAllBuscarNombreV();

            dgDatos.DataSource = dtBuscarV;
            dgDatos.Refresh();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarW = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarW = np.GetAllBuscarNombreW();

            dgDatos.DataSource = dtBuscarW;
            dgDatos.Refresh();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarX = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarX = np.GetAllBuscarNombreX();

            dgDatos.DataSource = dtBuscarX;
            dgDatos.Refresh();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarY = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarY = np.GetAllBuscarNombreY();

            dgDatos.DataSource = dtBuscarY;
            dgDatos.Refresh();
        }

        private void button24_Click(object sender, EventArgs e)
        {
            DataTable dtBuscarZ = new DataTable();
            clsNEGPerson np = new clsNEGPerson();
            dtBuscarZ = np.GetAllBuscarNombreZ();

            dgDatos.DataSource = dtBuscarZ;
            dgDatos.Refresh();
        }
    }
}
