﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using Laboratorio14__Tarea.Models;

namespace Laboratorio14__Tarea.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            return View(ma.RecuperarTodos());
            //return View();
        }




        public ActionResult Alta()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Alta(FormCollection collection)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            Articulo art = new Articulo
            {
                Codigo = int.Parse(collection["codigo"]),
                Nombre = collection["nombre"],
                Apellido = collection["apellido"],
                Descripcion = collection["descripcion"],
                Precio = float.Parse(collection["precio"].ToString()),
                Fechapedido = collection["fechapedido"]
            };
            ma.Alta(art);
            return RedirectToAction("Index");
        }

        public ActionResult Baja(int cod)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            ma.Borrar(cod);
            return RedirectToAction("Index");
        }

        public ActionResult Modificacion(int cod)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            Articulo art = ma.Recuperar(cod);
            return View(art);
        }

        [HttpPost]
        public ActionResult Modificacion(FormCollection collection)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();
            Articulo art = new Articulo
            {
                Codigo = int.Parse(collection["codigo"].ToString()),
                Nombre = collection["nombre"].ToString(),
                Apellido = collection["apellido"].ToString(),
                Descripcion = collection["descripcion"].ToString(),
                Precio = float.Parse(collection["precio"].ToString()),
                Fechapedido = collection["fechapedido"].ToString()


            };
            ma.Modificar(art);
            return RedirectToAction("Index");
        }

        // dE bUSQUEDA
        /* 
        public ActionResult BuscarArticulo()
        {
            return View();
        }
       */

        public ActionResult BuscarArticulo()

        {

            return View();

        }



        [HttpPost]
        public ActionResult BuscarArticulo(FormCollection coleccion)
        {
            MantenimientoArticulo ma = new MantenimientoArticulo();

          

              Articulo art = ma.Recuperar(int.Parse(coleccion["Codigo"].ToString()) );
            if (art != null)
                return View("Modificacion", art);
            else
                return View("ArticuloNoExistente");
        }












    }
}