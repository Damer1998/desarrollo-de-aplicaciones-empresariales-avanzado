
----  Creacion de la base de datos Practica2  - Alumno Abert Huarcaya Quispe
create database Practica2
go
use Practica2;

--- Tabla Clientes

CREATE TABLE clientes(
    PK_clientes int identity (1,1) primary key, 
	codcli int,
	nomcli nvarchar (60),
	telcli nvarchar (50),
	tiptar nvarchar (1),
	estado nvarchar (1)
)


/* 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clientes](
	[codcli] [int] NOT NULL,
	[nomcli] [varchar](60) NULL,
	[telcli] [varchar](50) NULL,
	[tiptar] [varchar](1) NULL,
	[estado] [varchar](1) NULL,
 CONSTRAINT [PK_clientes] PRIMARY KEY CLUSTERED 
(
	[codcli] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
*/


-------Insertar datos clientes
insert into clientes 
values
(1,'Solano','958852741','A','D'),
(2,'Neron','987652365','E','N'),
(3,'Samanta','952874273','A','D'),
(4,'Biba','952874271','E','N'),
(5,'Teresa','955255525','E','N'),
(6,'Devlin','952874270','A','N')



---------------- Tabla Registro --- Falla
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[registro](
	[invnum] [int] NOT NULL,
	[feccre] [datetime] NULL,
	[codcli] [int] NULL,
	[placa] [varchar](10) NULL,
	[fecini] [datetime] NULL,
	[fecfin] [datetime] NULL,
	[monto] [numeric](10, 2) NULL,
	[time] [time](7) NULL
) ON [PRIMARY]
GO


-------Insertar datos registro		 --- Falla
insert into registro 
values
(1,'2012-06-18 10:34:09.000',1001,'A40F50','2012-06-18 10:34:09.000','2012-06-18 10:34:09.000',45,'12:05:06.0000000'),
(2,'2014-06-18 10:34:09.000',1001,'B40F50','2014-06-18 10:34:09.000','2019-06-18 10:34:09.000',45,'12:05:06.0000000'),
(3,'2014-06-18 10:34:09.000',1002,'C80G80','2014-06-18 10:34:09.000','2019-06-18 10:34:09.000',45,'10:05:06.0000000'),
(4,'2014-06-18 10:34:09.000',1003,'D90F80','2014-06-18 10:34:09.000','2019-06-18 10:34:09.000',45,'10:05:06.0000000'),
(5,'2014-06-18 10:34:09.000',1004,'E10F00','2014-06-18 10:34:09.000','2019-06-18 10:34:09.000',45,'10:05:06.0000000'),
(6,'2014-06-18 10:34:09.000',1005,'F50F20','2014-06-18 10:34:09.000','2019-06-18 10:34:09.000',45,'10:05:06.0000000')

--------------------------Vehiculos --- Falla
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vehiculos](
	[codcli] [int] NOT NULL,
	[placa] [varchar](10) NOT NULL,
	[caracteristicas] [varchar](50) NULL,
	[estado] [varchar](1) NULL
) ON [PRIMARY]
GO


-------Insertar datos vehiculos --- Falla
insert into vehiculos 
values
(1,'A40F50','Todo Terreno','D'),
(2,'B40F50','Elegante','N'),
(3,'C80G80','Transitorio','D'),
(4,'D90F80','Elegante','N'),
(5,'E10F00','Inparcial','N'),
(6,'F50F20','Comercial','N')



---PROCEDIMIENTOS ALMACENADOS 
--------------------------MOSTRAR  
create proc MostrarClientes
as
select * from clientes
go

--------------------------INSERTAR --Nota  Se considera el  PK_clientes
create proc InsetarClientes
@codcli int,
@nomcli nvarchar(60),
@telcli nvarchar(50),
@tiptar nvarchar(1),
@estado nvarchar(1)
as
insert into clientes values (@codcli,@nomcli,@telcli,@tiptar,@estado)
go


------------------EDITAR  FALTA

create proc EditarClientes
@codcli int,
@nomcli nvarchar(60),
@telcli nvarchar(50),
@tiptar nvarchar(1),
@estado nvarchar(1),
@PK_clientes int
as
update clientes set codcli=@codcli, nomcli=@nomcli, telcli=@telcli, tiptar=@tiptar, estado=@estado where PK_clientes=@PK_clientes
go



------------------------ELIMINAR  PK_clientes
create proc EliminarClientes
@PK_clientes int
as
delete from clientes where PK_clientes=@PK_clientes
go


----- Ayuda
-- Eliminar Procedimientos
DROP PROCEDURE InsetarClientes;  
GO  