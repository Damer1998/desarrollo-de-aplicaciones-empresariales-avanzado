﻿using System;

namespace Laboratorio_ConsolaApp01_V1
{
    class Program
    {

        /*
        static void Main(string[] args)
        {
            Console.WriteLine("Ola prueba de Albert!");
            
            Suma(4,5);
            Raiz();
        }
        */

        static void Main(string[] args)
        {
            // Console. Title  Barra superior
            Console.Title = "Calculadora de Consola";
            string opcion;
            do
            {
                // Interfaz de consola Inicial
                Console.Clear();
                // Casos a esojer
                Console.WriteLine("  ");
                Console.WriteLine("  __ ^ __                                  __ ^ __ ");
                Console.WriteLine("   (___)------------------------------------(___)  ");
                Console.WriteLine("   | / |                                    | / |  ");
                Console.WriteLine("   | / |         Calculadora Basica         | / |  ");
                Console.WriteLine("   |___|                                    |___|  ");
                Console.WriteLine("  (_____)----------------------------------(_____) ");

                Console.WriteLine("  ");

                Console.WriteLine("     [1] Suma de dos números");
                Console.WriteLine("     [2] Resta de dos números");
                Console.WriteLine("     [3] Multiplicacion de dos números");
                Console.WriteLine("     [4] Division de dos números");

                Console.WriteLine("     [5] Imprimir la raíz de los 10 primeros ");
                Console.WriteLine("     [6] Imprimir los primos de los 10 primeros ");


                Console.WriteLine("     [7] Grados celsius  a fahrenheit");
                Console.WriteLine("     [8] Grados fahrenheit  a celsius");

                Console.WriteLine("     [0] Salir");


                Console.WriteLine("  ");
                Console.WriteLine("Ingrese una opción y presione ENTER");
                opcion = Console.ReadLine();
                switch (opcion)
                {
                    //Caso Suma
                    case "1":
                        Console.WriteLine("Ingrese el primer número");
                        int Amas = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingrese el segundo número");
                        int Bmas = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("     La suma de {0} y {1} es {2}", Amas, Bmas, Suma(Amas, Bmas));
                        Console.ReadKey();
                        break;

                    // Caso Resta
                    case "2":
                        Console.WriteLine("Ingrese el primer número");
                        int Amenos = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingrese el segundo número");
                        int Bmenos = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("     La resta  de {0} y {1} es {2}", Amenos, Bmenos, Resta(Amenos, Bmenos));
                        Console.ReadKey();
                        break;


                    case "3":
                        Console.WriteLine("Ingrese el primer número");
                        int Apor = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingrese el segundo número");
                        int Bpor = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("     La multiplicacion de {0} y {1} es {2}", Apor, Bpor, Multiplicar(Apor, Bpor));
                        Console.ReadKey();
                        break;


                    case "4":
                        Console.WriteLine("Ingrese el primer número");
                        int Aentre = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingrese el segundo número");
                        int Bentre = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("     La division de {0} y {1} es {2}", Aentre, Bentre, Dividir(Aentre, Bentre));
                        Console.ReadKey();
                        break;


                    case "5":
                        Console.WriteLine("     Calculando 10 primeros Numeros...");
                        Raiz();
                        Console.ReadKey();
                        break;



                    case "6":
                        Console.WriteLine("     Calculando 10 primeros Numeros...");
                        Primo();
                        Console.ReadKey();
                        break;



                    case "7":
                        Console.WriteLine("Ingrese la temperatura en grados celsius ");
                        double TempCelsius = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("     Son {1}F° ", TempCelsius, CaFahrenheit(TempCelsius));
                        Console.ReadKey();
                        break;

                    case "8":
                        Console.WriteLine("Ingrese la temperatura en grados fahrenheit ");
                        double TempFahrenheit = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("     Son {1}C° ", TempFahrenheit, FaCelsius(TempFahrenheit));
                        Console.ReadKey();
                        break;



                }
            } while (!opcion.Equals("0"));

        }
        // Double decimal o float
        // Clase estatica Float gradoFahrenheit a C
        static double FaCelsius(double TempFahrenheit)
        {

            return (TempFahrenheit - 32) * 5 / 9;
        }

        // Clase estatica Float gradoCelsius a F
        static double CaFahrenheit( double TempCelsius)
        {

            return (TempCelsius * 9 / 5) + 32;
        }



        //Clase estatica Dos Datos Suma
        static int Suma(int Amas, int Bmas)
        {
            return Amas + Bmas;
        }
        //Clase estatica Dos Datos Resta
        static int Resta( int Amenos, int Bmenos)
        {
            return Amenos - Bmenos;
        }
        //Clase estatica Dos Datos Multiplicar
        static int Multiplicar( int Apor, int Bpor)
        {
            //Nota la multiplicacion si aplica para cero
            return Apor * Bpor;
        }
        //Clase estatica Dos Datos Dividir
        static int Dividir( int Aentre, int Bentre)
            // Nota falta excion para dividr entre 0
        {
            return Aentre / Bentre;
        }






        // Raiz de los 10 primeros numeros
        static void Raiz()
        {
            for (int i = 1; i <= 10; i++)
            {
                //Clase estatica Imprime los diez veces
            //    Console.WriteLine("La raíz cuadrada de {0} es: {1}");
                //Procedimiento que imprime la raíz cuadrada de los 10 primeros números
                Console.WriteLine("La raíz cuadrada de {0} es: {1}", i, Math.Sqrt(i));
            }
        }

        // Primos de los 10 primeros numeos

        static void Primo()
           
        {
            int cont = 0;
            for (int i = 2; i <= 30; i++)
            {  

                for (int j = 1; j <= i; j++)
                {   
                    // Realiza comparacion
                    if (i % j == 0)
                    {
                        cont = cont + 1;
                    }
                }

                //si variable cont es mayor a dos, el numero es divisible en mas de dos numero osea no es primo
                if (cont <= 2)
                { 
                    Console.WriteLine(i);
                }      

                //igualamos a cero para hacer otra evaluacion con otro numero
                cont = 0;               


            }
        }








    }
}
