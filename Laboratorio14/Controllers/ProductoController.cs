﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using Laboratorio14.Models;

namespace Laboratorio14.Controllers
{
    public class ProductoController : Controller
    {

        NegociosEntities db = new NegociosEntities();

        // GET: Producto
        public ActionResult Index()
        {
            return View(db.productos.ToList());
        }

        public ActionResult IndexBusqueda(string producto = null)
        {
            IQueryable<productos> resultado = db.productos;
            if (!string.IsNullOrEmpty(producto))
            {
                resultado = resultado.Where(p => p.NomProducto.Contains(producto));

            }
            return View(resultado.ToList());
        }

    }
}