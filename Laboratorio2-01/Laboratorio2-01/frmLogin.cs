﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorio2_01
{
    public partial class frmLogin : Form
    {
        private int intentos;

        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }


        // Botones para iniciar sesion

        private void btnIniciar_Click(object sender, EventArgs e)
        {

            /* INGRESO DIRECTO 
             // Redirecciona el MDI
            // EjemploMDI principal = new EjemploMDI();
             PrincipalMDI principal = new PrincipalMDI();
             principal.Show();
             this.Hide();

             */

            // Declar 


            
            string[,] arreglo = { { "ola", "Daka" }, { "2013", "damer28" } };

            // int intentos = 0;

            if (txtUsuario.Text == arreglo[0, 0] && txtPassword.Text == arreglo[1, 0])
            {
                this.Hide();
                PrincipalMDI principal = new PrincipalMDI();
                principal.Show();
            }
            else if (intentos == 3)
            {
              
                MessageBox.Show("Ha realizado varios intentos, Porfavor vuelva a intentarlo mas tarde", "Mensaje del sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else
            {
                intentos +=1 ;
                MessageBox.Show("Error de autenticacion, verifique usuario o contraseña ", "Mensaje del sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPassword.Text = "";
                txtUsuario.Text = "";

            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
