﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorio2_01
{
    public partial class PrincipalMDI : Form
    {
        public PrincipalMDI()
        {
            InitializeComponent();
        }

        private void mantenimientoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        // SubMenu Usuario
        private void mnuManUsuarios_click(object sender, EventArgs e)
        {
            manUsuario frm = new manUsuario();
            frm.MdiParent = this;
            frm.Show();

        }

        // sALIR
        private void mnuSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void sistemaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
