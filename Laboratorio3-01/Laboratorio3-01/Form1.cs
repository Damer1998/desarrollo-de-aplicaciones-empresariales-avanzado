﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Conexion
using System.Data.SqlClient;

namespace Laboratorio3_01
{
    public partial class Form1 : Form
    {
        // SQLConnection permite el manejo de acceso del servidor
        SqlConnection conn;

        public Form1()
        {
            InitializeComponent();
        }



        // Boton Conectar
        private void btnConectar_Click(object sender, EventArgs e)
        {
            // Delarar varaible para almacenar los valores de los TetBox y definir cadena de Conexion
            String servidor = txtServidor.Text;
            String bd = txtBaseDatos.Text;
            String user = txtUsuario.Text;
            String pwd = txtPassword.Text;

            String str = "Server=" + servidor + ";DataBase=" + bd + ";";

            // La cadena de conexion cambia en funcion del estado de CehckBox
            if (chkAutenticacion.Checked)
                str += "Integrated Security=true";

            else
                str += "User Id=" + user + ";Password=" + pwd + ";";

            try
            {
                conn = new SqlConnection(str);
                conn.Open();
                MessageBox.Show("Conectado Sastifactoriamente");
                btnDesconectar.Enabled = true;

            }
            catch(Exception ex)
            {
                MessageBox.Show("Error al conectar el servidor: \n " + ex.ToString());
            }

        }

        // Boton de estado
        private void btnEstado_Click(object sender, EventArgs e)
        {
            // Obtener estado de Conexion y en esete caso abierta
            // Recuperacion de informacion de la misma

            try
            {
                if (conn.State == ConnectionState.Open)
                    MessageBox.Show("El estaod del servidor: " + conn.State +
                        "\nVersion del servidor: " + conn.ServerVersion +
                        "\nBase de datos: " + conn.Database);


                else
                    MessageBox.Show("Estado de servicior : " + conn.State);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Imposible determinar el estado del servidor: \n " +
                    ex.ToString());
            }

        }

        //Boton Desconectar
        private void btnDesconectar_Click(object sender, EventArgs e)
        {

            //Verificacion de Conexion
            try
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                    MessageBox.Show("Conexion cerrada sastifactoriamente");
                }
                else
                    MessageBox.Show("La conexion ya esta cerrada");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al cerrar la conexion: \n " + ex.ToString());
            }


        }

        // CheckBoX
        private void chkAutenticacion_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAutenticacion.Checked)
            {
                txtUsuario.Enabled = false;
                txtPassword.Enabled = false;
            }
            else
            {
                txtUsuario.Enabled = true;
                txtPassword.Enabled = true;
            }
        }

        // Boton Personas  Ingreso a sesion
        private void btnPersona_Click(object sender, EventArgs e)
        {
            Persona persona = new Persona(conn);
            persona.Show();

        }






    }
}
