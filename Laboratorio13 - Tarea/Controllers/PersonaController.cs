﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Laboratorio13___Tarea.Models;

namespace Laboratorio13___Tarea.Controllers
{
    public class PersonaController : Controller
    {
        // GET: Persona
        public ActionResult Index(string name)
        {
            if (string.IsNullOrEmpty(name))
                return View(clientes);
            else
            {
                ViewBag.Name = name;
                // //   return View(clientes.Where(c => c.Correo.ToLower().Contains(name)       )); // Para un dato
                // Para dos datos
                return View(clientes.Where(c => c.Nombre.ToLower().Contains(name) || c.Correo.ToLower().Contains(name) ));
            }


        }


        private static readonly List<Persona> clientes = new List<Persona>()
        {
             new Persona { ClienteId = 1, Nombre = "Julio Avellaneda", Correo = "julito_gtu@hotmail.com"},
             new Persona { ClienteId = 2, Nombre = "Juan Torres", Correo = "jtorres@hotmail.com"},
             new Persona { ClienteId = 3, Nombre = "Oscar Camacho", Correo = "oscarca@hotmail.com"},
             new Persona { ClienteId = 4, Nombre = "Gina Urrego", Correo = "ginna@hotmail.com"},
             new Persona { ClienteId = 5, Nombre = "Albert Huarcaya", Correo = "natha@hotmail.com"},
             new Persona { ClienteId = 6, Nombre = "Raul Rodriguez", Correo = "rodriguez.raul@hotmail.com"},
              new Persona { ClienteId = 7, Nombre = "Johana Espitia", Correo = "johana_espitia@hotmail.com"}


        };




    }
}


