﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



// Usar capa negocio
using negocio;

namespace Laboratorio12_Practica3
{
    public partial class registro : MaterialSkin.Controls.MaterialForm
    {


        //  Private
        private N_Clientes objetoCN = new N_Clientes();
        private string Correlativo = null;  // primary key 
        private bool Editar = false;






        public registro()
        {
            InitializeComponent();
        }




        // Listado Inicio y Actulizado
        private void MostrarCliente()
        {

            N_Clientes objeto = new N_Clientes();
            dataGridView1.DataSource = objeto.MostrarCliente();
        }

        // Registro
        private void panel_Load(object sender, EventArgs e)
        {

            MostrarCliente();
        }







        //Limpieza de Datos
        private void limpiarForm()
        {

            txtCorrelativo.Clear();
            txtCodigoOA.Clear();
            txtPaciente.Clear();
            txtEmail.Clear();
            txtCelular.Clear();
            txtMedico.Clear();
            txtAseguradora.Clear();
            txtProcedencia.Clear();
            txtExamen.Clear();
            txtEstado.Clear();
            txtEdad.Clear();
            txtImagen.Clear();
            txtInforme.Clear();
            //    txtFecha.Clear();







        }


        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //INSERTAR
            if (Editar == false)
            {
                try
                {
                    objetoCN.InsertarCliente(
                    txtCodigoOA.Text,
                    txtPaciente.Text,
                    txtEmail.Text,
                    txtCelular.Text,
                    txtMedico.Text,
                    txtAseguradora.Text,
                    txtProcedencia.Text,
                    txtExamen.Text,
                    txtEstado.Text,
                    txtFecha.Text,
                    txtEdad.Text,
                    txtImagen.Text,
                    txtInforme.Text
                          );
                    MessageBox.Show("El registro del paciente a sido agregado correctamente");
                    MostrarCliente();
                    limpiarForm();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se pudo insertar los datos por: " + ex);
                }
            }
            //EDITAR
            if (Editar == true)
            {

                try
                {
                    objetoCN.EditarCliente(
                    txtCodigoOA.Text,
                    txtPaciente.Text,
                    txtEmail.Text,
                    txtCelular.Text,
                    txtMedico.Text,
                    txtAseguradora.Text,
                    txtProcedencia.Text,
                    txtExamen.Text,
                    txtEstado.Text,
                    txtFecha.Text,
                    txtEdad.Text,
                    txtImagen.Text,
                    txtInforme.Text,
                    Correlativo
                        );
                    MessageBox.Show("Se edito registro del paciente a sido agregado correctamente");
                    MostrarCliente();
                    MostrarCliente();
                    limpiarForm();
                    Editar = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se pudo editar los datos por: " + ex);
                }
            }


        }


        private void brnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Editar = true;
 
                txtPaciente.Text = dataGridView1.CurrentRow.Cells["Paciente"].Value.ToString(); //1
                txtMedico.Text = dataGridView1.CurrentRow.Cells["Medico"].Value.ToString();  //2
                txtFecha.Text = dataGridView1.CurrentRow.Cells["Fecha_solicitud"].Value.ToString(); //3
                txtExamen.Text = dataGridView1.CurrentRow.Cells["Examen"].Value.ToString(); // 4
                txtCodigoOA.Text = dataGridView1.CurrentRow.Cells["CodigoOA"].Value.ToString(); // 5
                txtEdad.Text = dataGridView1.CurrentRow.Cells["Edad"].Value.ToString();  // 6
                txtAseguradora.Text = dataGridView1.CurrentRow.Cells["Aseguradora"].Value.ToString(); //7
                txtProcedencia.Text = dataGridView1.CurrentRow.Cells["Procedencia"].Value.ToString(); //8
                txtEmail.Text = dataGridView1.CurrentRow.Cells["Email"].Value.ToString(); // 9
                txtCelular.Text = dataGridView1.CurrentRow.Cells["Celular"].Value.ToString(); // 10
                txtEstado.Text = dataGridView1.CurrentRow.Cells["Estado"].Value.ToString();  // 11
                txtImagen.Text = dataGridView1.CurrentRow.Cells["Imagen"].Value.ToString(); //12
                txtInforme.Text = dataGridView1.CurrentRow.Cells["Informe"].Value.ToString(); // 13

                Correlativo = dataGridView1.CurrentRow.Cells["Correlativo"].Value.ToString(); // 1



            }
            else
                MessageBox.Show("Seleccione una fila por favor");
        }


        // Eliminar
        private void btnEliminar_Click(object sender, EventArgs e)
        {
    
                if (dataGridView1.SelectedRows.Count > 0)
                {
                    Correlativo = dataGridView1.CurrentRow.Cells["Correlativo"].Value.ToString();
                    objetoCN.EliminarCliente(Correlativo);
                    MessageBox.Show("Eliminado Paciente correctamente");
                    MostrarCliente();
                }
                else
                    MessageBox.Show("Seleccione una fila por favor");
          
        }


    }
}
