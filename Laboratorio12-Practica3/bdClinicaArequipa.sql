USE [Practica3]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- [datetime] NULL,
CREATE TABLE [dbo].[clientes](
	[Correlativo] [int] IDENTITY(1,1) NOT NULL,
	[CodigoOA] [int] NULL,
	[Paciente] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[Celular] [int] NULL,
	[Medico] [nvarchar](100) NULL,
	[Aseguradora] [nvarchar](100) NULL,
	[Procedencia] [nvarchar](100) NULL,
	[Examen] [nvarchar](100) NULL,
	[Estado] [nvarchar](1) NULL,
	[Fecha_solicitud] [nvarchar](100) NULL,
	[Edad] [int] NULL,
	[Imagen] [nvarchar](100) NULL,
	[Informe] [nvarchar](100) NULL,

PRIMARY KEY CLUSTERED 
(
	[Correlativo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


---- Segunda Ejecucion
----Procedimiento Listar
create proc MostrarPacientes
as
select * from clientes
go
-- Se omite correlativo
---Tercera Ejecucion
--- Insertar @Fecha_solicitud datetime,
create proc InsetarPacientes
@CodigoOA int,
@Paciente nvarchar(100),
@Email nvarchar(100),
@Celular int,
@Medico nvarchar(100),
@Aseguradora nvarchar(100),
@Procedencia nvarchar(100),
@Examen nvarchar(100),
@Estado nvarchar(1),
@Fecha_solicitud nvarchar(100),
@Edad  int,
@Imagen nvarchar(100),
@Informe nvarchar(100)
as
insert into clientes values (@CodigoOA,@Paciente,@Email,@Celular,@Medico,@Aseguradora,@Procedencia,@Examen,@Estado,@Fecha_solicitud,@Edad,@Imagen,@Informe)
go

---- Cuarta Ejcuacion
---Editar

create proc EditarPacientes
@CodigoOA int,
@Paciente nvarchar(100),
@Email nvarchar(100),
@Celular int,
@Medico nvarchar(100),
@Aseguradora nvarchar(100),
@Procedencia nvarchar(100),
@Examen nvarchar(100),
@Estado nvarchar(1),
@Fecha_solicitud nvarchar(100),
@Edad  int,
@Imagen nvarchar(100),
@Informe nvarchar(100),
@Correlativo int
as
update clientes set 
@CodigoOA=CodigoOA,
@Paciente=Paciente,
@Email=Email,
@Celular=Celular,
@Medico=Medico,
@Aseguradora=Aseguradora,
@Procedencia=Procedencia,
@Examen=Examen,
@Estado=Estado ,
@Fecha_solicitud=Fecha_solicitud,
@Edad=Edad,
@Imagen=Imagen ,
@Informe=Informe  
where Correlativo=@Correlativo 
go


--- Eliminar Paciente
create proc EliminarPacientes
@Correlativo int
as
delete from clientes where Correlativo=@Correlativo 
go




----- Ayuda
-- Eliminar Procedimientos
DROP PROCEDURE InsetarPacientes;  
GO  
