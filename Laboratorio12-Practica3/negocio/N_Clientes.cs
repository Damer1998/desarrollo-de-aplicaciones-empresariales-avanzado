﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



using System.Data;
using System.Data.SqlClient;
// Uso de CApa
using datos;


namespace negocio
{
    public class N_Clientes
    {

        private D_Clientes objetoCD = new D_Clientes();

        public DataTable MostrarCliente()
        {

            DataTable tabla = new DataTable();
            tabla = objetoCD.Mostrar();
            return tabla;
        }



        public void InsertarCliente(
            string CodigoOA,
            string Paciente,
            string Email,
            string Celular,
            string Medico,
            string Aseguradora,
            string Procedencia,
            string Examen,
            string Estado,
            string Fecha_solicitud,
            string Edad,
            string Imagen,
            string Informe)
        {

            objetoCD.Insertar(
                Convert.ToInt32(CodigoOA),
                 Paciente,
                 Email,
                 Convert.ToInt32(Celular),
                 Medico,
                 Aseguradora,
                 Procedencia,
                 Examen,
                 Estado,
                Convert.ToDateTime(Fecha_solicitud),
                Convert.ToInt32(Edad),
                Imagen,
                Informe
               
                );
            //  objetoCD.Insertar(nombre,desc,marca,Convert.ToDouble(precio),Convert.ToInt32(stock)); float
        }
        

        public void EditarCliente(
            string CodigoOA,
            string Paciente,
            string Email,
            string Celular,
            string Medico,
            string Aseguradora,
            string Procedencia,
            string Examen,
            string Estado,
            string Fecha_solicitud,
            string Edad,
            string Imagen,
            string Informe,
            string Correlativo)
        {
            objetoCD.Editar(
                 Convert.ToInt32(CodigoOA),
                 Paciente,
                 Email,
                 Convert.ToInt32(Celular),
                 Medico,
                 Aseguradora,
                 Procedencia,
                 Examen,
                 Estado,
                Convert.ToDateTime(Fecha_solicitud),
                Convert.ToInt32(Edad),
                Imagen,
                Informe,
                 Convert.ToInt32(Correlativo));
        }




        public void EliminarCliente(string Correlativo)
        {

            objetoCD.Eliminar(Convert.ToInt32(Correlativo));
        }

    }
}






/*
 * 
 *         public void InsertarCliente(string CodigoOA, string Paciente, string Medico, string Aseguradora, string Procedencia,
            string Examen, string Fecha_solicitud, string Edad)
        {

            objetoCD.Insertar(Convert.ToInt32(CodigoOA), Paciente, Medico, Aseguradora, Procedencia, 
                Examen, Convert.ToDateTime(Fecha_solicitud) , Convert.ToInt32(Edad));
            //  objetoCD.Insertar(nombre,desc,marca,Convert.ToDouble(precio),Convert.ToInt32(stock)); float
        }
        

        public void EditarCliente(string CodigoOA, string Paciente, string Medico, string Aseguradora, string Procedencia,
            string Examen, string Fecha_solicitud, string Edad , string Correlativo)
        {
            objetoCD.Editar(Convert.ToInt32(CodigoOA), Paciente, Medico, Aseguradora, Procedencia,
                Examen, Convert.ToDateTime(Fecha_solicitud), Convert.ToInt32(Edad) , Convert.ToInt32(Correlativo));
        }
 * 
 * */
