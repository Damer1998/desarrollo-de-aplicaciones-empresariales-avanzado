﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// SQL
using System.Data.SqlClient;
using System.Data;

namespace datos
{
    public class D_Clientes
    {

        // Listar Clientes
        private CD_Conexion conexion = new CD_Conexion();

        SqlDataReader leer;
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();


        // Procedimiento MostrarPacientes   -- Listo sin defectos
        public DataTable Mostrar()
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "MostrarPacientes";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;

        }



        public void Insertar(
            int CodigoOA, 
            string Paciente, 
            string Email,
            int Celular,
            string Medico, 
            string Aseguradora, 
            string Procedencia ,
            string Examen , 
            string Estado ,
            DateTime Fecha_solicitud , 
            int Edad,
            string Imagen,
            string Informe)
        {
            //PROCEDIMNIENTO

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "InsetarPacientes";
            comando.CommandType = CommandType.StoredProcedure;

            // 8 Datos para registros
            comando.Parameters.AddWithValue("@CodigoOA", CodigoOA);
            comando.Parameters.AddWithValue("@Paciente", Paciente);
            comando.Parameters.AddWithValue("@Email", Email);
            comando.Parameters.AddWithValue("@Email", Celular);
            comando.Parameters.AddWithValue("@Medico", Medico);
            comando.Parameters.AddWithValue("@Aseguradora", Aseguradora);
            comando.Parameters.AddWithValue("@Procedencia", Procedencia);
            comando.Parameters.AddWithValue("@Examen", Examen);
            comando.Parameters.AddWithValue("@Examen", Estado);
            comando.Parameters.AddWithValue("@Fecha_solicitud", Fecha_solicitud);
            comando.Parameters.AddWithValue("@Edad", Edad);
            comando.Parameters.AddWithValue("@Edad", Imagen);
            comando.Parameters.AddWithValue("@Edad", Informe);



            comando.ExecuteNonQuery();

            comando.Parameters.Clear();

        }
        /*  */
        public void Editar(
            int CodigoOA,
            string Paciente,
            string Email,
            int Celular,
            string Medico,
            string Aseguradora,
            string Procedencia,
            string Examen,
            string Estado,
            DateTime Fecha_solicitud,
            int Edad,
            string Imagen,
            string Informe,
            int Correlativo)
        {

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EditarPacientes";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@CodigoOA", CodigoOA);
            comando.Parameters.AddWithValue("@Paciente", Paciente);
            comando.Parameters.AddWithValue("@Email", Email);
            comando.Parameters.AddWithValue("@Email", Celular);
            comando.Parameters.AddWithValue("@Medico", Medico);
            comando.Parameters.AddWithValue("@Aseguradora", Aseguradora);
            comando.Parameters.AddWithValue("@Procedencia", Procedencia);
            comando.Parameters.AddWithValue("@Examen", Examen);
            comando.Parameters.AddWithValue("@Examen", Estado);
            comando.Parameters.AddWithValue("@Fecha_solicitud", Fecha_solicitud);
            comando.Parameters.AddWithValue("@Edad", Edad);
            comando.Parameters.AddWithValue("@Edad", Imagen);
            comando.Parameters.AddWithValue("@Edad", Informe);
            comando.Parameters.AddWithValue("@Correlativo", Correlativo);
            comando.ExecuteNonQuery();

            comando.Parameters.Clear();
        }


        //  -- Eliminar  sin defectos
        public void Eliminar(int Correlativo)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "EliminarPacientes";
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@Correlativo", Correlativo);

            comando.ExecuteNonQuery();

            comando.Parameters.Clear();
        }




    }
}


/*
 * 
        public void Insertar(int CodigoOA, string Paciente, string Medico, string Aseguradora, string Procedencia ,
            string Examen , DateTime Fecha_solicitud , int Edad)


        public void Editar(int CodigoOA, string Paciente, string Medico, string Aseguradora, string Procedencia,
            string Examen, DateTime Fecha_solicitud, int Edad , int Correlativo)

*/