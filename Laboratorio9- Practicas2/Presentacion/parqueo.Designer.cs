﻿namespace Presentacion
{
    partial class parqueo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(parqueo));
            this.btnReimprimirTicket = new System.Windows.Forms.Button();
            this.btnIngresoCliente = new System.Windows.Forms.Button();
            this.btnIngresoPlaca = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.labelTema = new System.Windows.Forms.Label();
            this.txtplacas = new System.Windows.Forms.TextBox();
            this.plagascli = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnIngreso = new System.Windows.Forms.Button();
            this.btnSalida = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReimprimirTicket
            // 
            this.btnReimprimirTicket.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnReimprimirTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReimprimirTicket.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReimprimirTicket.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnReimprimirTicket.Location = new System.Drawing.Point(770, 297);
            this.btnReimprimirTicket.Name = "btnReimprimirTicket";
            this.btnReimprimirTicket.Size = new System.Drawing.Size(219, 46);
            this.btnReimprimirTicket.TabIndex = 91;
            this.btnReimprimirTicket.Text = "Reimprimir Ticket";
            this.btnReimprimirTicket.UseVisualStyleBackColor = false;
            this.btnReimprimirTicket.Click += new System.EventHandler(this.btnReimprimirTicket_Click);
            // 
            // btnIngresoCliente
            // 
            this.btnIngresoCliente.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnIngresoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIngresoCliente.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngresoCliente.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnIngresoCliente.Location = new System.Drawing.Point(770, 239);
            this.btnIngresoCliente.Name = "btnIngresoCliente";
            this.btnIngresoCliente.Size = new System.Drawing.Size(219, 46);
            this.btnIngresoCliente.TabIndex = 90;
            this.btnIngresoCliente.Text = "Nuevo Cliente";
            this.btnIngresoCliente.UseVisualStyleBackColor = false;
            this.btnIngresoCliente.Click += new System.EventHandler(this.btnIngresoCliente_Click);
            // 
            // btnIngresoPlaca
            // 
            this.btnIngresoPlaca.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnIngresoPlaca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIngresoPlaca.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngresoPlaca.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnIngresoPlaca.Location = new System.Drawing.Point(770, 186);
            this.btnIngresoPlaca.Name = "btnIngresoPlaca";
            this.btnIngresoPlaca.Size = new System.Drawing.Size(219, 46);
            this.btnIngresoPlaca.TabIndex = 88;
            this.btnIngresoPlaca.Text = "Placa Nueva";
            this.btnIngresoPlaca.UseVisualStyleBackColor = false;
            this.btnIngresoPlaca.Click += new System.EventHandler(this.btnIngresoPlaca_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(41, 438);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1377, 404);
            this.dataGridView1.TabIndex = 87;
            // 
            // labelTema
            // 
            this.labelTema.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTema.BackColor = System.Drawing.Color.Red;
            this.labelTema.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTema.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelTema.Location = new System.Drawing.Point(-4, 0);
            this.labelTema.Name = "labelTema";
            this.labelTema.Size = new System.Drawing.Size(703, 42);
            this.labelTema.TabIndex = 82;
            this.labelTema.Text = "Herramienta Rapida - Parqueo";
            this.labelTema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtplacas
            // 
            this.txtplacas.Location = new System.Drawing.Point(127, 71);
            this.txtplacas.Name = "txtplacas";
            this.txtplacas.Size = new System.Drawing.Size(266, 22);
            this.txtplacas.TabIndex = 84;
            // 
            // plagascli
            // 
            this.plagascli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plagascli.ForeColor = System.Drawing.Color.White;
            this.plagascli.Location = new System.Drawing.Point(3, 67);
            this.plagascli.Name = "plagascli";
            this.plagascli.Size = new System.Drawing.Size(104, 31);
            this.plagascli.TabIndex = 83;
            this.plagascli.Text = "Placas";
            this.plagascli.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SeaGreen;
            this.panel2.Controls.Add(this.labelTema);
            this.panel2.Controls.Add(this.plagascli);
            this.panel2.Controls.Add(this.txtplacas);
            this.panel2.Location = new System.Drawing.Point(41, 186);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(699, 155);
            this.panel2.TabIndex = 89;
            // 
            // btnIngreso
            // 
            this.btnIngreso.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnIngreso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIngreso.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngreso.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnIngreso.Location = new System.Drawing.Point(120, 360);
            this.btnIngreso.Name = "btnIngreso";
            this.btnIngreso.Size = new System.Drawing.Size(241, 34);
            this.btnIngreso.TabIndex = 92;
            this.btnIngreso.Text = "Ingreso";
            this.btnIngreso.UseVisualStyleBackColor = false;
            // 
            // btnSalida
            // 
            this.btnSalida.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSalida.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalida.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalida.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnSalida.Location = new System.Drawing.Point(424, 360);
            this.btnSalida.Name = "btnSalida";
            this.btnSalida.Size = new System.Drawing.Size(241, 34);
            this.btnSalida.TabIndex = 93;
            this.btnSalida.Text = "Salida";
            this.btnSalida.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(1026, 186);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(392, 155);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 94;
            this.pictureBox1.TabStop = false;
            // 
            // parqueo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1449, 888);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSalida);
            this.Controls.Add(this.btnIngreso);
            this.Controls.Add(this.btnReimprimirTicket);
            this.Controls.Add(this.btnIngresoCliente);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnIngresoPlaca);
            this.Controls.Add(this.dataGridView1);
            this.Name = "parqueo";
            this.Text = "Sistema de Parqueo";
            this.Load += new System.EventHandler(this.parqueo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnReimprimirTicket;
        private System.Windows.Forms.Button btnIngresoCliente;
        private System.Windows.Forms.Button btnIngresoPlaca;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label labelTema;
        private System.Windows.Forms.TextBox txtplacas;
        private System.Windows.Forms.Label plagascli;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnIngreso;
        private System.Windows.Forms.Button btnSalida;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}