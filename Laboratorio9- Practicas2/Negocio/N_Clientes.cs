﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Data;
using System.Data.SqlClient;
// Uso de CApa
using Datos;


namespace Negocio
{
    public class N_Clientes
    {

        private D_Clientes objetoCD = new D_Clientes();

        public DataTable MostrarCliente()
        {

            DataTable tabla = new DataTable();
            tabla = objetoCD.Mostrar();
            return tabla;
        }


       
        public void InsertarCliente(string codcli, string nomcli, string telcli, string tiptar, string estado)
        {

            objetoCD.Insertar(Convert.ToInt32(codcli) , nomcli, telcli, tiptar, estado );
            //  objetoCD.Insertar(nombre,desc,marca,Convert.ToDouble(precio),Convert.ToInt32(stock)); float
        }


        public void EditarCliente(string codcli, string nomcli, string telcli, string tiptar, string estado , string PK_clientes)
       {
           objetoCD.Editar(Convert.ToInt32(codcli), nomcli, telcli, tiptar, estado , Convert.ToInt32(PK_clientes));
       }

       

        public void EliminarCliente(string PK_clientes)
        {

            objetoCD.Eliminar(Convert.ToInt32(PK_clientes));
        }
        
    }
}
