﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Diseño
using MaterialSkin;


namespace Laboratorio_8
{
    public partial class Principal : MaterialSkin.Controls.MaterialForm
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void Principal_Load(object sender, EventArgs e)
        {

        }

        private void btnPerson_Click(object sender, EventArgs e)
        {
            Persona dirigepersona = new Persona();
            dirigepersona.Show();

            /*
            lista persona = new lista();
            persona.Show();
            this.Close();
            */
        }

        private void btnCurso_Click(object sender, EventArgs e)
        {
            Cursos dirigecurso = new Cursos();
            dirigecurso.Show();
        }

        private void btnCursoGrado_Click(object sender, EventArgs e)
        {
            CursoGrado dirigecurso = new CursoGrado();
            dirigecurso.Show();
        }

        private void btnDepartamento_Click(object sender, EventArgs e)
        {
            Departamento dirigedepartamento = new Departamento();
            dirigedepartamento.Show();
        }

        private void btnCursoInstructor_Click(object sender, EventArgs e)
        {
            CursoInstructor dirigecursoinstructor = new CursoInstructor();
            dirigecursoinstructor.Show();
        }

        private void btnOficceAsignament_Click(object sender, EventArgs e)
        {
            OficioAsignamento dirigeAsignamietno = new OficioAsignamento();
            dirigeAsignamietno.Show();
        }

        private void btnOnline_Click(object sender, EventArgs e)
        {
            CursoOnline dirigecursoOnline = new CursoOnline();
            dirigecursoOnline.Show();
        }

        private void btnOnsiteCourse_Click(object sender, EventArgs e)
        {
            CursoPresencial diregePrecensial = new CursoPresencial();
            diregePrecensial.Show();
        }
    }
}
