﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño
using MaterialSkin;

namespace Laboratorio_8
{
    public partial class CursoPresencial : MaterialSkin.Controls.MaterialForm
    {
        public CursoPresencial()
        {
            InitializeComponent();
        }

        private void onsiteCourseBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.onsiteCourseBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.escuelaDataSet);

        }

        private void CursoPresencial_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'escuelaDataSet.OnsiteCourse' Puede moverla o quitarla según sea necesario.
            this.onsiteCourseTableAdapter.Fill(this.escuelaDataSet.OnsiteCourse);

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
