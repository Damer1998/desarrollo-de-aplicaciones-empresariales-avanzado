﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño
using MaterialSkin;

namespace Laboratorio_8
{
    public partial class Departamento : MaterialSkin.Controls.MaterialForm
    {
        public Departamento()
        {
            InitializeComponent();
        }

        private void Departamento_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'escuelaDataSet.Department' Puede moverla o quitarla según sea necesario.
            this.departmentTableAdapter.Fill(this.escuelaDataSet.Department);

        }

        private void departmentBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.departmentBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.escuelaDataSet);

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
