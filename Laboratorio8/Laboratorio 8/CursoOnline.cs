﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Diseño
using MaterialSkin;


namespace Laboratorio_8
{
    public partial class CursoOnline : MaterialSkin.Controls.MaterialForm
    {
        public CursoOnline()
        {
            InitializeComponent();
        }

        private void CursoOnline_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'escuelaDataSet.OnlineCourse' Puede moverla o quitarla según sea necesario.
            this.onlineCourseTableAdapter.Fill(this.escuelaDataSet.OnlineCourse);

        }

        private void onlineCourseBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.onlineCourseBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.escuelaDataSet);

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
