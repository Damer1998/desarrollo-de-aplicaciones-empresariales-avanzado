﻿namespace Laboratorio_8
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCurso = new System.Windows.Forms.Button();
            this.btnCursoInstructor = new System.Windows.Forms.Button();
            this.btnCursoGrado = new System.Windows.Forms.Button();
            this.btnOnline = new System.Windows.Forms.Button();
            this.btnOficceAsignament = new System.Windows.Forms.Button();
            this.btnPerson = new System.Windows.Forms.Button();
            this.btnOnsiteCourse = new System.Windows.Forms.Button();
            this.btnDepartamento = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCurso
            // 
            this.btnCurso.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnCurso.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCurso.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCurso.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnCurso.Location = new System.Drawing.Point(54, 117);
            this.btnCurso.Name = "btnCurso";
            this.btnCurso.Size = new System.Drawing.Size(244, 37);
            this.btnCurso.TabIndex = 74;
            this.btnCurso.Text = "Curso";
            this.btnCurso.UseVisualStyleBackColor = false;
            this.btnCurso.Click += new System.EventHandler(this.btnCurso_Click);
            // 
            // btnCursoInstructor
            // 
            this.btnCursoInstructor.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnCursoInstructor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCursoInstructor.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCursoInstructor.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnCursoInstructor.Location = new System.Drawing.Point(54, 203);
            this.btnCursoInstructor.Name = "btnCursoInstructor";
            this.btnCursoInstructor.Size = new System.Drawing.Size(244, 37);
            this.btnCursoInstructor.TabIndex = 79;
            this.btnCursoInstructor.Text = "Curso Instructor";
            this.btnCursoInstructor.UseVisualStyleBackColor = false;
            this.btnCursoInstructor.Click += new System.EventHandler(this.btnCursoInstructor_Click);
            // 
            // btnCursoGrado
            // 
            this.btnCursoGrado.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnCursoGrado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCursoGrado.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCursoGrado.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnCursoGrado.Location = new System.Drawing.Point(54, 160);
            this.btnCursoGrado.Name = "btnCursoGrado";
            this.btnCursoGrado.Size = new System.Drawing.Size(244, 37);
            this.btnCursoGrado.TabIndex = 78;
            this.btnCursoGrado.Text = "Curso Grado";
            this.btnCursoGrado.UseVisualStyleBackColor = false;
            this.btnCursoGrado.Click += new System.EventHandler(this.btnCursoGrado_Click);
            // 
            // btnOnline
            // 
            this.btnOnline.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnOnline.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOnline.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOnline.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnOnline.Location = new System.Drawing.Point(338, 160);
            this.btnOnline.Name = "btnOnline";
            this.btnOnline.Size = new System.Drawing.Size(244, 37);
            this.btnOnline.TabIndex = 81;
            this.btnOnline.Text = "Curso Onliene";
            this.btnOnline.UseVisualStyleBackColor = false;
            this.btnOnline.Click += new System.EventHandler(this.btnOnline_Click);
            // 
            // btnOficceAsignament
            // 
            this.btnOficceAsignament.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnOficceAsignament.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOficceAsignament.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOficceAsignament.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnOficceAsignament.Location = new System.Drawing.Point(338, 117);
            this.btnOficceAsignament.Name = "btnOficceAsignament";
            this.btnOficceAsignament.Size = new System.Drawing.Size(244, 37);
            this.btnOficceAsignament.TabIndex = 80;
            this.btnOficceAsignament.Text = "Oficio de Asignamiento";
            this.btnOficceAsignament.UseVisualStyleBackColor = false;
            this.btnOficceAsignament.Click += new System.EventHandler(this.btnOficceAsignament_Click);
            // 
            // btnPerson
            // 
            this.btnPerson.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnPerson.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPerson.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPerson.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnPerson.Location = new System.Drawing.Point(338, 246);
            this.btnPerson.Name = "btnPerson";
            this.btnPerson.Size = new System.Drawing.Size(244, 37);
            this.btnPerson.TabIndex = 83;
            this.btnPerson.Text = "Persona";
            this.btnPerson.UseVisualStyleBackColor = false;
            this.btnPerson.Click += new System.EventHandler(this.btnPerson_Click);
            // 
            // btnOnsiteCourse
            // 
            this.btnOnsiteCourse.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnOnsiteCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOnsiteCourse.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOnsiteCourse.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnOnsiteCourse.Location = new System.Drawing.Point(338, 203);
            this.btnOnsiteCourse.Name = "btnOnsiteCourse";
            this.btnOnsiteCourse.Size = new System.Drawing.Size(244, 37);
            this.btnOnsiteCourse.TabIndex = 82;
            this.btnOnsiteCourse.Text = "Curso Presencial";
            this.btnOnsiteCourse.UseVisualStyleBackColor = false;
            this.btnOnsiteCourse.Click += new System.EventHandler(this.btnOnsiteCourse_Click);
            // 
            // btnDepartamento
            // 
            this.btnDepartamento.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnDepartamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepartamento.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepartamento.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDepartamento.Location = new System.Drawing.Point(54, 246);
            this.btnDepartamento.Name = "btnDepartamento";
            this.btnDepartamento.Size = new System.Drawing.Size(244, 37);
            this.btnDepartamento.TabIndex = 84;
            this.btnDepartamento.Text = "Departamento";
            this.btnDepartamento.UseVisualStyleBackColor = false;
            this.btnDepartamento.Click += new System.EventHandler(this.btnDepartamento_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 360);
            this.Controls.Add(this.btnDepartamento);
            this.Controls.Add(this.btnPerson);
            this.Controls.Add(this.btnOnsiteCourse);
            this.Controls.Add(this.btnOnline);
            this.Controls.Add(this.btnOficceAsignament);
            this.Controls.Add(this.btnCursoInstructor);
            this.Controls.Add(this.btnCursoGrado);
            this.Controls.Add(this.btnCurso);
            this.Name = "Principal";
            this.Text = "Plataforma de Mantenimiento School";
            this.Load += new System.EventHandler(this.Principal_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCurso;
        private System.Windows.Forms.Button btnCursoInstructor;
        private System.Windows.Forms.Button btnCursoGrado;
        private System.Windows.Forms.Button btnOnline;
        private System.Windows.Forms.Button btnOficceAsignament;
        private System.Windows.Forms.Button btnPerson;
        private System.Windows.Forms.Button btnOnsiteCourse;
        private System.Windows.Forms.Button btnDepartamento;
    }
}