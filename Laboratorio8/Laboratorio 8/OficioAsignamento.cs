﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño
using MaterialSkin;

namespace Laboratorio_8
{
    public partial class OficioAsignamento : MaterialSkin.Controls.MaterialForm
    {
        public OficioAsignamento()
        {
            InitializeComponent();
        }

        private void OficioAsignamento_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'escuelaDataSet.OfficeAssignment' Puede moverla o quitarla según sea necesario.
            this.officeAssignmentTableAdapter.Fill(this.escuelaDataSet.OfficeAssignment);

        }

        private void officeAssignmentBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.officeAssignmentBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.escuelaDataSet);

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
