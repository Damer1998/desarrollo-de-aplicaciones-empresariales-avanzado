﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Diseño
using MaterialSkin;

namespace Laboratorio_8
{
    public partial class Persona : MaterialSkin.Controls.MaterialForm
    {
        public Persona()
        {
            InitializeComponent();
        }

        private void personBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.personBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.escuelaDataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'escuelaDataSet.Person' Puede moverla o quitarla según sea necesario.
            this.personTableAdapter.Fill(this.escuelaDataSet.Person);

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {

            this.Close();
        }
    }
}
