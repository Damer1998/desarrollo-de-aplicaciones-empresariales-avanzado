﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Diseño
using MaterialSkin;


namespace Laboratorio_8
{
    public partial class CursoGrado : MaterialSkin.Controls.MaterialForm
    {
        public CursoGrado()
        {
            InitializeComponent();
        }

        private void CursoGrado_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'escuelaDataSet.CourseGrade' Puede moverla o quitarla según sea necesario.
            this.courseGradeTableAdapter.Fill(this.escuelaDataSet.CourseGrade);

        }

        private void courseGradeBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.courseGradeBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.escuelaDataSet);

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
