﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño
using MaterialSkin;


namespace Laboratorio_8
{
    public partial class Cursos :  MaterialSkin.Controls.MaterialForm
    {
        public Cursos()
        {
            InitializeComponent();
        }

        private void courseBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.courseBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.escuelaDataSet);

        }

        private void Cursos_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'escuelaDataSet.Course' Puede moverla o quitarla según sea necesario.
            this.courseTableAdapter.Fill(this.escuelaDataSet.Course);

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
