﻿namespace Laboratorio_8
{
    partial class CursoInstructor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CursoInstructor));
            this.escuelaDataSet = new Laboratorio_8.EscuelaDataSet();
            this.courseInstructorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.courseInstructorTableAdapter = new Laboratorio_8.EscuelaDataSetTableAdapters.CourseInstructorTableAdapter();
            this.tableAdapterManager = new Laboratorio_8.EscuelaDataSetTableAdapters.TableAdapterManager();
            this.courseInstructorBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.courseInstructorBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.courseInstructorDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSalir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.escuelaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseInstructorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseInstructorBindingNavigator)).BeginInit();
            this.courseInstructorBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.courseInstructorDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // escuelaDataSet
            // 
            this.escuelaDataSet.DataSetName = "EscuelaDataSet";
            this.escuelaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // courseInstructorBindingSource
            // 
            this.courseInstructorBindingSource.DataMember = "CourseInstructor";
            this.courseInstructorBindingSource.DataSource = this.escuelaDataSet;
            // 
            // courseInstructorTableAdapter
            // 
            this.courseInstructorTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CourseGradeTableAdapter = null;
            this.tableAdapterManager.CourseInstructorTableAdapter = this.courseInstructorTableAdapter;
            this.tableAdapterManager.CourseTableAdapter = null;
            this.tableAdapterManager.DepartmentTableAdapter = null;
            this.tableAdapterManager.OfficeAssignmentTableAdapter = null;
            this.tableAdapterManager.OnlineCourseTableAdapter = null;
            this.tableAdapterManager.OnsiteCourseTableAdapter = null;
            this.tableAdapterManager.PersonTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Laboratorio_8.EscuelaDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // courseInstructorBindingNavigator
            // 
            this.courseInstructorBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.courseInstructorBindingNavigator.BindingSource = this.courseInstructorBindingSource;
            this.courseInstructorBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.courseInstructorBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.courseInstructorBindingNavigator.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.courseInstructorBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.courseInstructorBindingNavigatorSaveItem});
            this.courseInstructorBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.courseInstructorBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.courseInstructorBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.courseInstructorBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.courseInstructorBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.courseInstructorBindingNavigator.Name = "courseInstructorBindingNavigator";
            this.courseInstructorBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.courseInstructorBindingNavigator.Size = new System.Drawing.Size(849, 27);
            this.courseInstructorBindingNavigator.TabIndex = 0;
            this.courseInstructorBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorAddNewItem.Text = "Agregar nuevo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(48, 24);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de elementos";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorDeleteItem.Text = "Eliminar";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primero";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posición";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posición actual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveNextItem.Text = "Mover siguiente";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // courseInstructorBindingNavigatorSaveItem
            // 
            this.courseInstructorBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.courseInstructorBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("courseInstructorBindingNavigatorSaveItem.Image")));
            this.courseInstructorBindingNavigatorSaveItem.Name = "courseInstructorBindingNavigatorSaveItem";
            this.courseInstructorBindingNavigatorSaveItem.Size = new System.Drawing.Size(29, 24);
            this.courseInstructorBindingNavigatorSaveItem.Text = "Guardar datos";
            this.courseInstructorBindingNavigatorSaveItem.Click += new System.EventHandler(this.courseInstructorBindingNavigatorSaveItem_Click);
            // 
            // courseInstructorDataGridView
            // 
            this.courseInstructorDataGridView.AutoGenerateColumns = false;
            this.courseInstructorDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.courseInstructorDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.courseInstructorDataGridView.DataSource = this.courseInstructorBindingSource;
            this.courseInstructorDataGridView.Location = new System.Drawing.Point(30, 90);
            this.courseInstructorDataGridView.Name = "courseInstructorDataGridView";
            this.courseInstructorDataGridView.RowHeadersWidth = 51;
            this.courseInstructorDataGridView.RowTemplate.Height = 24;
            this.courseInstructorDataGridView.Size = new System.Drawing.Size(793, 355);
            this.courseInstructorDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CourseID";
            this.dataGridViewTextBoxColumn1.HeaderText = "CourseID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "PersonID";
            this.dataGridViewTextBoxColumn2.HeaderText = "PersonID";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 125;
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.Crimson;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnSalir.Location = new System.Drawing.Point(579, 467);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(244, 37);
            this.btnSalir.TabIndex = 85;
            this.btnSalir.Text = "Regresar";
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // CursoInstructor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 530);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.courseInstructorDataGridView);
            this.Controls.Add(this.courseInstructorBindingNavigator);
            this.Name = "CursoInstructor";
            this.Text = "Mantenimiento de Curso Instructor";
            this.Load += new System.EventHandler(this.CursoInstructor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.escuelaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseInstructorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseInstructorBindingNavigator)).EndInit();
            this.courseInstructorBindingNavigator.ResumeLayout(false);
            this.courseInstructorBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.courseInstructorDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private EscuelaDataSet escuelaDataSet;
        private System.Windows.Forms.BindingSource courseInstructorBindingSource;
        private EscuelaDataSetTableAdapters.CourseInstructorTableAdapter courseInstructorTableAdapter;
        private EscuelaDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator courseInstructorBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton courseInstructorBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView courseInstructorDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button btnSalir;
    }
}