﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño
using MaterialSkin;

namespace Laboratorio_8
{
    public partial class CursoInstructor : MaterialSkin.Controls.MaterialForm
    {
        public CursoInstructor()
        {
            InitializeComponent();
        }

        private void courseInstructorBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.courseInstructorBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.escuelaDataSet);

        }

        private void CursoInstructor_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'escuelaDataSet.CourseInstructor' Puede moverla o quitarla según sea necesario.
            this.courseInstructorTableAdapter.Fill(this.escuelaDataSet.CourseInstructor);

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
